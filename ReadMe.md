**Install the following dependencies**

*Plugins included in this app*

Network Provider
```sh
ionic cordova plugin add cordova-plugin-network-information
```
```sh
npm install --save @ionic-native/network
```
Spinner

```sh
npm install ngx-spinner@v6.0.0 --save
```
Toast

```sh
ionic cordova plugin add cordova-plugin-x-toast
```
```sh
npm install --save @ionic-native/toast
```
Install AngularFire and Firebase

```sh
npm i firebase@4.12.1 angularfire2@5.0.0-rc.4 --save
```

```sh
npm i --save lodash
```
Google Maps

```sh
ionic cordova plugin add https://github.com/mapsplugin/cordova-plugin-googlemaps --variable API_KEY_FOR_ANDROID="AIzaSyB7rKS6iYSSYbN9pn8OJe4ce3RrkJDvHig"
```
```sh
npm install --save @ionic-native/google-maps
```

GRO
```sh
gmrprimegro@hial.com
gro@123
```
GRL
```sh
gmrprimegrl@hial.com
grl@123
```




