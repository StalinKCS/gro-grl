export const environment = {
    production: false,
    // firebase: {
    //     apiKey: "AIzaSyB22GIoxLBFlpYzl14uZ_fpFdOTfzexKws",
    //     authDomain: "gmr-prime-dev.firebaseapp.com",
    //     databaseURL: "https://gmr-prime-dev.firebaseio.com",
    //     projectId: "gmr-prime-dev",
    //     storageBucket: "gmr-prime-dev.appspot.com",
    //     messagingSenderId: "551812956594",
    //     baseUrl: "https://us-central1-applied-craft-217711.cloudfunctions.net/",
    //     version:"0.0.1"
    // }
    firebase:{
        apiKey: "AIzaSyBeYI6Xx-rprUpL34qbfvJJwub4nHV4hc8",
        authDomain: "gmr-prime.firebaseapp.com",
        databaseURL: "https://gmr-prime.firebaseio.com",
        projectId: "gmr-prime",
        storageBucket: "gmr-prime.appspot.com",
        messagingSenderId: "869210279658",
        baseUrl: "https://us-central1-applied-craft-217711.cloudfunctions.net/",
        sender_id: "869210279658",
        oneSignalAppId: "160d50df-cfce-4b81-b30b-4a052c7609c0",
        webClientId: "869210279658-mdevidttpegfqkj82dos1olbcite0qk7.apps.googleusercontent.com",
        version:"0.0.4",
        paymentUrl:"https://test.gmrprimepay.hyderabad.aero"
    }
};