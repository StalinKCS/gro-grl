export const environment = {
    production: true,
    firebase: {
        apiKey: "AIzaSyDdL21mczNZR8H2xaJSsbyyQjhahRsGqc0",
        authDomain: "applied-craft-217711.firebaseapp.com",
        databaseURL: "https://applied-craft-217711.firebaseio.com",
        projectId: "applied-craft-217711",
        storageBucket: "applied-craft-217711.appspot.com",
        messagingSenderId: "107597237454",
        baseUrl: "https://us-central1-applied-craft-217711.cloudfunctions.net/",
        version:"0.0.1"
    }
};