import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ExecutiveDetailsPage } from '../executive-details/executive-details';

/**
 * Generated class for the SlotStatusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-slot-status',
  templateUrl: 'slot-status.html',
})
export class SlotStatusPage {
  slotStatus = 'pending_slot';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SlotStatusPage');
  }
  gotonext(){
    this.navCtrl.push(ExecutiveDetailsPage);
  }
}
