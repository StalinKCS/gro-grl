import { Component, Renderer } from '@angular/core';
import { NavController, NavParams,ViewController } from 'ionic-angular';

/**
 * Generated class for the PopMeetGreetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pop-meet-greet',
  templateUrl: 'pop-meet-greet.html',
})
export class PopMeetGreetPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,public renderer: Renderer, public viewCtrl: ViewController) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'modal-otp', true) 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopMeetGreetPage');
  }
  closeModal(){
    this.viewCtrl.dismiss();
  }
}
