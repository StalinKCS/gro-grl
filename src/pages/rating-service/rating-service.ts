import { Component, Renderer } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as moment from 'moment-timezone';
// import { Ionic2RatingModule } from "ionic2-rating";
@Component({
  selector: 'page-rating-service',
  templateUrl: 'rating-service.html',
})
export class RatingServicePage {
  rate: any = {};
  rateDes: any;
  msg: any = "";
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public afDB: AngularFireDatabase) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'modal-rating', true)
    this.rateDes = [
      { name: "Overall Review" }
    ];
  }

  submitRating() {
    console.log(this.rate);
    console.log(this.rate["0"]);
    // console.log(this.msg)
    if (!this.rate["0"]) {
      let alert = this.alertCtrl.create({
        title: 'Please Rate The Guest',
        buttons: ['Dismiss']
      });
      alert.present();
    } else {
      let bookingID = this.navParams.get('bookingID');
      let mode = this.navParams.get("mode");
      let idx = this.navParams.get("idx")
      var d = moment.tz('Asia/Kolkata').toString();
      var serviceItemStatusPath = "/bookingDetails/" + mode + "/" + bookingID + "/serviceList/" + idx + "/checked";
      var serviceItemCompletedAtPath = "/bookingDetails/" + mode + "/" + bookingID + "/serviceList/" + idx + "/completedAt";
      console.log(bookingID)
      var update = {};
      var bookingDetailsRatingPath = "/bookingDetails/" + mode + "/" + bookingID + "/rating";
      var ratingObj = { "ratedByGRO": { "stars": this.rate[0], "msg": this.msg } };
      update[serviceItemStatusPath] = "true";
      update[serviceItemCompletedAtPath] = d;
      update[bookingDetailsRatingPath] = ratingObj;
      this.afDB.object("/").update(update);
      let data = {
        "msg": "success"
      }
      this.viewCtrl.dismiss(data);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RatingServicePage');
  }

  ionViewWillEnter() {
    let bookingID = this.navParams.get('bookingID');
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
}
