import { ChangePasswordPage } from './../change-password/change-password';
import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase, AngularFireObject } from 'angularfire2/database';
import { NgxSpinnerService } from 'ngx-spinner';
import { ForgotPwdPage } from '../forgot-pwd/forgot-pwd';
import * as _ from 'lodash';
import { OtpPage } from '../otp/otp';
import { FetcherServiceProvider } from '../../providers/fetcher-service/fetcher-service';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UtilsProvider } from '../../providers/utils/utils';
@Component({
  selector: 'page-mg-login',
  templateUrl: 'mg-login.html',
})
export class MgLoginPage {
  // loginForm: FormGroup;
  // loginError: string;
  user: AngularFireObject<any[]>;
  userObj: any;
  email: any;
  password: any;

  constructor(public fetcher: FetcherServiceProvider, public spinner: NgxSpinnerService
    , public db: AngularFireDatabase, public afAuth: AngularFireAuth, public navCtrl: NavController,
    public splashScreen: SplashScreen,
    public utils: UtilsProvider,
    public navParams: NavParams) {
    // this.loginForm = fb.group({
    //   email: [null, Validators.compose([Validators.required, Validators.email])],
    //   password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    // });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MgLoginPage');
  }
  ionViewDidEnter() {
    setTimeout(() => {
      if (this.splashScreen) {
        this.splashScreen.hide();
      }
    }, 6000);

  }


  _toOpen() {
    this.navCtrl.push(ForgotPwdPage);
  }

  isEmpty(val) {
    return (val === false || val === '' || val === undefined || val == null || val.length <= 0) ? true : false;
  }

  login() {
    var vm = this;
    // let data = vm.loginForm.value;
    // if (!data.email) {
    //   return;
    // }
    var regEmail = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
    if (vm.isEmpty(vm.email)) {
      vm.utils.makeToast('Please enter your Email ID.');
    } else if (!regEmail.test(vm.email.trim())) {
      vm.utils.makeToast('Please enter a valid Email ID.');
    } else if (vm.isEmpty(vm.password)) {
      vm.utils.makeToast('Please enter your password');
    }
    else {
      vm.spinner.show();
      vm.afAuth.auth.signInWithEmailAndPassword(vm.email.trim(), vm.password).then((val) => {
        var uid = val.uid
        // Reminder: When a staff is added from Admin Dashboard, following should be added in the below path
        // staffDetails/roleInfo/staffs/uid/{"role":"gro"}
        var roleInfoPath = 'staffDetails/roleInfo/staffs/' + uid;
        // console.log(roleInfoPath)
        vm.db.object(roleInfoPath).valueChanges().subscribe(res => {
          // console.log(res)
          var roleType = res;
          var staffDetailPath = 'staffDetails/' + res + "/" + uid;
          vm.db.object(staffDetailPath).valueChanges().subscribe(res => {
            vm.spinner.hide();
            if (!res["isActivated"]) {
              vm.navCtrl.push(ChangePasswordPage, { "uid": uid, "roleType": roleType });
            }
            else {
              if (!localStorage.getItem("otp")) {
                vm.navCtrl.push(OtpPage, { "mobileNo": res["mobileNo"], "role": res["role"], "uId": uid, "groName": res["name"] });
              }
            }
          })
        })
      }).catch(
        (error) => {
          this.spinner.hide();
          vm.utils.makeToast(error.message);
          // console.log(error.message);
          // this.loginError = error.message;
        })
    }
  }
}
