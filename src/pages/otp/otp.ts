import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FetcherServiceProvider } from '../../providers/fetcher-service/fetcher-service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DashGroPage } from '../dash-gro/dash-gro';
import { DashGrlPage } from '../dash-grl/dash-grl';
import { MgLoginPage } from '../mg-login/mg-login';
import { ExecutiveDetailsPage } from '../executive-details/executive-details';
import { AlertController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {
  otpForm: FormGroup;
  mobileNo: any;
  role: any;
  uId: any;
  groName:any;
  loginError: string;
  grList = [];
  isOTPSent: boolean = false;

  constructor(public alertCtrl: AlertController, public spinner: NgxSpinnerService, public fetcher: FetcherServiceProvider, public navCtrl: NavController, public navParams: NavParams, public fb: FormBuilder, public db: AngularFireDatabase) {

    this.mobileNo = navParams.get("mobileNo");
    this.role = navParams.get("role");
    this.uId = navParams.get("uId");
    this.groName = navParams.get("groName");
    this.otpForm = fb.group({
      mob_number: ['', Validators.compose([Validators.required])],
      otp: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern('[0-9]*')])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
    var vm = this;
    var roleInfoPath = 'staffDetails/' + this.role;
    vm.db.object(roleInfoPath).valueChanges().subscribe(res => {
      Object.keys(res).forEach((key) => {
        if (vm.grList.indexOf(res[key].mobileNo) == -1)
          vm.grList.push(res[key].mobileNo);
      });
    });
    console.log(vm.grList);
  }

  mobileSelected() {
    this.otpForm.get('otp').setValue('');
    this.isOTPSent = false;
  }

  sendOtp() {
    var vm = this;
    vm.spinner.show();
    console.log(vm.mobileNo);
    let payload = {
      "mType": "osov",
      "mobNo": vm.mobileNo,
      "otp": "no"
    }
    vm.fetcher.sendOtp(payload).then(resp => {
      if (resp["type"] == 'success') {
        vm.isOTPSent = true;
      }
      else {
        vm.loginError = resp["type"];
        vm.isOTPSent = false;
      }
      vm.spinner.hide();
    })
  }

  resendOtp() {
    var vm = this;
    vm.spinner.show();
    let payload = {
      "mType": "osov",
      "mobNo": vm.mobileNo,
      "otp": "no"
    }
    vm.fetcher.sendOtp(payload).then(resp => {
      vm.spinner.hide();
    })
  }

  // Getting the date on YYYYMMDD format
  _yyyymmdd(slotDate) {
    var x = new Date(slotDate);
    var y = x.getFullYear().toString();
    var m = (x.getMonth() + 1).toString();
    var d = x.getDate().toString();
    (d.length == 1) && (d = '0' + d);
    (m.length == 1) && (m = '0' + m);
    var yyyymmdd = y + m + d;
    return yyyymmdd;
  }

  verifyOtp() {
    var vm = this;
    let data = vm.otpForm.value;
    if (!data.otp) {
      return;
    }
    vm.spinner.show();
    let payload = {
      "mType": "osov",
      "mobNo": vm.mobileNo,
      "otp": data.otp
    }
    vm.fetcher.verifyOtp(payload).then(resp => {
      vm.spinner.hide();
      console.log(resp);
      if (resp["type"] == 'success') {
        console.log(vm.role);
        if (vm.role == 'gro') {
          localStorage.setItem("otp", "verified");
          var staffDetailPath = 'staffDetails/' + vm.role + "/" + vm.uId + '/loggedDetails/' + vm._yyyymmdd(new Date()) + "/" + new Date().getTime();
          var staffDetailPath_1 = 'staffDetails/' + vm.role + "/" + vm.uId +'/mobileNo';
          let update = {};
          var loggedDetails = {
            "mobileNo": vm.mobileNo,
            "lastSession": new Date().getTime()
          }
          update[staffDetailPath] = loggedDetails;
          update[staffDetailPath_1] = vm.mobileNo;
          vm.db.object('/').update(update);
          vm.navCtrl.setRoot(DashGroPage);
        }
        else if (vm.role == 'grl') {
          localStorage.setItem("otp", "verified");
          vm.navCtrl.setRoot(ExecutiveDetailsPage);
        }
        else {
          vm.navCtrl.setRoot(MgLoginPage);
        }
      }
      else {
        const alert = this.alertCtrl.create({
          title: 'Alert!',
          subTitle: 'Invalid OTP',
          buttons: ['OK']
        });
        alert.present();
      }
    })
  }

}
