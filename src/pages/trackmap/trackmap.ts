import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ModalController, Modal, ModalOptions, Platform } from 'ionic-angular';
import { RatingServicePage } from '../rating-service/rating-service';
import { ReportsPage } from '../reports/reports';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import * as firebase from 'firebase';
import { AngularFireDatabase } from 'angularfire2/database';
// import {  CallNumber } from '@ionic-native/call-number';



declare var google;
/**
 * Generated class for the TrackmapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-trackmap',
  templateUrl: 'trackmap.html',
})
export class TrackmapPage {
  // Start is end position and end is start position. Since last minute chnage is applied
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  bookingID: any;
  directionsDisplay: any;
  directionsService: any;
  guestName: string;
  markers = [];
  routes = [];
  perLat: any;
  perLong: any;
  watch: any;
  guestPhone: any;

  start: string = 'Hatti Kaapi, Shamshabad, Hyderabad, Telangana, India';

  // start: any; //= 'Madurai, Tamil Nadu';   //'Newgen Digitalworks P Ltd, LB Road, Thiruvanmiyur, Chennai, Tamil Nadu';
  end: any;//= 'Chennai, Tamil Nadu'; //'Newgen KnowledgeWorks Private Limited, Ranjith Nagar, Neelankarai, Chennai, Tamil Nadu';
  distancetime = "calculating distance";
  currentMapTrack = null;
  bookingStatus: any;
  serviceList: any;
  constructor(
    public geolocation: Geolocation,
    public navCtrl: NavController,
    public platform: Platform,
    public afDB: AngularFireDatabase,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    private diagnostic: Diagnostic, private locationAccuracy: LocationAccuracy
  ) {
    platform.ready().then(() => {
      if (this.diagnostic.isGpsLocationEnabled()) {
        this.diagnostic.isGpsLocationEnabled().then((enabled) => {
          console.log('Gps enabled');
          this.gpsEnabledFunction(enabled);
        }, (error) => {
          console.error("The following error occurred: " + error);
        });
      }
      else {
        this.diagnostic.isLocationEnabled().then((enabled) => {
          console.log('Location enabled');
          this.gpsEnabledFunction(enabled);
        }, (error) => {
          console.error("The following error occurred: " + error);
        });

      }

    });
    //  this.initMap();
    this.bookingStatus = navParams.get("bookingStatus");
    this.serviceList = navParams.get("serviceList");
    this.bookingID = navParams.get("bookingID");
    this.guestName = navParams.get("guestName");
    this.guestPhone = navParams.get("guestPhone");
  }
  gpsEnabledFunction(enabled) {
    // console.log("GPS location is " + (enabled ? "enabled" : "disabled"));
    if (enabled) {
      this.initMap();

    }
    else {
      this.locationAccuracy.canRequest().then((canRequest: boolean) => {
        if (canRequest) {
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
            // console.log('Request successful');

            this.initMap();


          }, (error) => {
            console.log('Error requesting location permissions', error);
          })
        }
      })

    }
  }
  openModal() {
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: false,
    };
    const myModal: Modal = this.modalCtrl.create(RatingServicePage, myModalOptions);
    myModal.present();
    myModal.onDidDismiss((data) => {
      console.log("I have dismissed.");
      console.log(data);
    });
    myModal.onWillDismiss((data) => {
      console.log("I'm about to dismiss");
      console.log(data);
    });
  }
  previousPage() {
    this.navCtrl.pop();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad TrackmapPage');

  }
  nextPage() {
    this.navCtrl.setRoot(ReportsPage);
  }


  initMap() {

    this.geolocation.getCurrentPosition().then((resp) => {
      //let mylocation = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
     let mylocation = new google.maps.LatLng(17.314480, 78.446120);
      this.map = new google.maps.Map(this.mapElement.nativeElement, {
        center: mylocation,
        zoom: 20,
        disableDefaultUI: true,
        mapTypeControl: false,
        gestureHandling: 'greedy',
        

      });
      // this.start = mylocation;
      this.addClientmarker();
      this.setMapOnAll(this.map);

    });
   

  }

  addClientmarker() {

    var geoTrackingPath = "/geoTracking/" + this.bookingID;
    console.log(geoTrackingPath);
    this.afDB.object(geoTrackingPath).valueChanges().subscribe(res => {
      console.log(res);
      if (res) {

        let clientlocation = new google.maps.LatLng(res["latitude"], res["longitude"]);
        console.log(clientlocation);
        this.end = clientlocation;
        this.displayRoute();
      }
      // return false;
    })

  }


  addMarker(location, image) {

    let marker = new google.maps.Marker({
      position: location,
      map: this.map,
      icon: image
    });
    this.markers.push(marker);
  }

  setMapOnAll(map) {

    for (var i = 0; i < this.markers.length; i++) {
      this.markers[i].setMap(map);
    }
  }

  clearMarkers() {
    this.setMapOnAll(null);
  }

  deleteMarkers() {
    this.setMapOnAll(null);
    this.markers = [];
  }



  redrawPath(path, map) {
    let startImg = 'assets/imgs/dote.png';
    let destImg = 'assets/imgs/car.png';
    this.deleteMarkers();
    this.addMarker(this.start,startImg );
    this.addMarker(this.end, destImg);
    let tempArr = [];
    tempArr.push(this.start);
    tempArr.push(this.end);
    var latLngBounds = new google.maps.LatLngBounds();
    for (var i = 0; i < tempArr.length; i++) {
      latLngBounds.extend(tempArr[i]);
    }
    if (this.currentMapTrack) {
      this.currentMapTrack.setMap(null);
    }

    if (path.length > 1) {
      var lineSymbol = {
        path: google.maps.SymbolPath.CIRCLE,
        fillOpacity: 1,
        scale: 3
      };
      this.currentMapTrack = new google.maps.Polyline({
        path: path,
        strokeColor: '#000',
        strokeOpacity: 0,
        icons: [{
          icon: lineSymbol,
          offset: '0',
          repeat: '20px'
        }]
      });
      this.currentMapTrack.setMap(this.map);
      this.map.fitBounds(latLngBounds);
    }
  }

  displayRoute() {

    var latLngBounds = new google.maps.LatLngBounds();
    for (var i = 0; i < this.markers.length; i++) {
      latLngBounds.extend(this.markers[i].getPosition());
    }
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer({ map: this.map });
    var polylinecoords = [];

    this.directionsService.route({
      origin: this.end,
      destination: this.start,
      drivingOptions: {
        departureTime: new Date(Date.now()),
        trafficModel: 'optimistic'
      },
      travelMode: 'DRIVING',
    }, (response, status) => {

      if (status === 'OK') {
        console.log(response);
        var legs = response.routes[0].legs;
        for (var i = 0; i < legs.length; i++) {
          var steps = legs[i].steps;
          for (var j = 0; j < steps.length; j++) {
            var nextSegment = steps[j].path;
            for (var k = 0; k < nextSegment.length; k++) {

              this.distancetime = response.routes[0].legs[0].duration.text;
              this.start = response.routes[0].legs[0].end_location;
              this.end = response.routes[0].legs[0].start_location;

              console.log(this.distancetime);
              polylinecoords.push(nextSegment[k]);
            }

          }
        }
        this.redrawPath(polylinecoords, this.map);
        console.log(response.routes[0].legs[0].distance.text);
        


      }
      else {
        // window.alert('Directions request failed due to ' + status);
      }
    });



  }
 

  // callNumber() {
  //   this.call.callNumber("+91 " + this.guestPhone, true)
  //     .then(res => console.log('Launched dialer!', res))
  //     .catch(err => console.log('Error launching dialer', err));
  // }

}
