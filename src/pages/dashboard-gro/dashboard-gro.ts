import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, Modal, ModalOptions } from 'ionic-angular';
import { PopStartServicePage } from '../pop-start-service/pop-start-service';

@Component({
  selector: 'page-dashboard-gro',
  templateUrl: 'dashboard-gro.html',
})
export class DashboardGroPage {
  userObj: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
  }

  openModal() {
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: false,
    };

    const myModal: Modal = this.modalCtrl.create(PopStartServicePage, myModalOptions);

    myModal.present();

    myModal.onDidDismiss((data) => {
      console.log("I have dismissed.");
      console.log(data);
    });

    myModal.onWillDismiss((data) => {
      console.log("I'm about to dismiss");
      console.log(data);
    });

  }
  ionViewDidLoad() {

  }
}
