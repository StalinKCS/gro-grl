import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PrimePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-prime',
  templateUrl: 'prime.html',
})
export class PrimePage {
  bookingInfo:any;
  checkList:any;
  checkedVal: any = {};
  // = [
  //               {"title":"GUEST RECEIVED"},
  //               {"title":"OTP VERIFIED"}
  //             ];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.bookingInfo = navParams.get("bookingInfo");
    
    var items = [
                  {"title":"GUEST RECEIVED","checked":"false","key":"guestReceived"},
                  {"title":"OTP VERIFIED","checked":"false","key":"otpVerified"},
                  {"title":"CHECK IN","checked":"false","key":"checkIn"},
                  {"title":"SECURITY","checked":"false","key":"security"},
                  {"title":"IMMIGRATION","checked":"false","key":"immigration"},
                  {"title":"SERVICE COMPLETED","checked":"false","key":"serviceCompleted"}
                ]
      this.checkList = items;
    // if(this.bookingInfo["addOnDetails"]){
      
    // }
  }

  updateStatus(idx,key){
    this.checkList[idx]["checked"] = "true";
    // for(var i in this.checkList){
    //   if(this.checkList[i]["key"] === key){
    //     this.checkList[i]["checked"] = true;
    //   }
    // }
    console.log(this.checkList)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PrimePage');
  }

}
