import { Component, Directive, Output, EventEmitter, Input, SimpleChange } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { SlotStatusPage } from '../slot-status/slot-status';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { TrackmapPage } from '../trackmap/trackmap';
/**
 * Generated class for the DashGrlPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dash-grl',
  templateUrl: 'dash-grl.html',
})

export class DashGrlPage {
  grlID: string;
  slotsList = [];
  groList = [];
  dash_overview = "gro";
  shownGroup = null;
  shownSlot = null;
  slots_overview = "scheduledSlots";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private afDB: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    public alertCtrl: AlertController) {
    this.afAuth.authState.subscribe((auth) => {
      if (auth) {
        this.grlID = auth.uid;
        var bookingDetailsPath = "/bookingDetails";
        var groListPath = "/staffDetails/gro";
        this.afDB.list(groListPath).valueChanges().subscribe(res => {
          this.groList = res;
          console.log(this.groList);
        })
        this.afDB.object(bookingDetailsPath).valueChanges().subscribe(res => {
          Object.keys(res).forEach(key => {

            if (key != "departurePrivilegePlan") {
              var mode = res[key];
              Object.keys(mode).forEach(key => {
                var status = mode[key]["status"];
                this.slotsList.push(mode[key]);
              })

            }
          })
        })
      }

    })
  }

  
  trackGuest(status,serviceList,bookingId,firstName,lastName,mobileNo)
  {
  this.navCtrl.push(TrackmapPage, { "bookingStatus": status,"serviceList": serviceList,"bookingID": bookingId, "guestName": firstName + " " + lastName, "guestPhone": mobileNo });
  }

  toDetect(uid) {
    console.log(uid);
    let todaDate = new Date().getDate();
    if (this.groList.hasOwnProperty(uid)) {
      return this.groList[uid].schedule[todaDate - 1].shift;
    }
    else {
      return 'None'
    }
  }


  guestCount(a, c, i) {
    var totalGuest = 0;
    totalGuest = parseInt(a) + parseInt(c) + parseInt(i);
    return totalGuest;
  }



  convertToLowerCase(addOnArray){
    var a;
    var newArray = []
    for(a of addOnArray){
      newArray.push(a.toLowerCase());
    }
    return newArray;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashGrlPage');
  }
  gotonext() {
    this.navCtrl.push(SlotStatusPage);
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };

  toggleSlot(group) {
    if (this.isSlotShown(group)) {
      this.shownSlot = null;
    } else {
      this.shownSlot = group;
    }
  };

  isSlotShown(group) {
    return this.shownSlot === group;
  };

  slotCount(groID, slotType) {
    var slotTypeId = slotType + groID;
    console.log(slotTypeId);
  }

  slotChk(groID, slotType) {
    console.log(groID, slotType)
  }

}
