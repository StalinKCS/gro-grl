import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, Modal, ModalOptions, Events } from 'ionic-angular';
// import { Platform } from 'ionic-angular';
import * as firebase from 'firebase/app';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AngularFireAuth } from 'angularfire2/auth';
import { Http, RequestOptions, Request, RequestMethod, Headers } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { map } from 'rxjs/operators';
import { TrackmapPage } from '../trackmap/trackmap';
import { TrackGuestPage } from '../track-guest/track-guest';
import { PlacardPage } from '../placard/placard';
import { RatingServicePage } from '../rating-service/rating-service';
import { PrimePage } from '../prime/prime';
import * as moment from 'moment-timezone';
import { PopStartServicePage } from '../pop-start-service/pop-start-service';
import { PauseModalPage } from '../pause-modal/pause-modal';
import { FetcherServiceProvider } from '../../providers/fetcher-service/fetcher-service';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { Diagnostic } from '@ionic-native/diagnostic';
import { SplashScreen } from '@ionic-native/splash-screen';

/**
 * Generated class for the DashGroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-dash-gro',
  templateUrl: 'dash-gro.html',
})
export class DashGroPage {
  groID: string;
  oneSignalUrl = "https://onesignal.com/api/v1/notifications";
  playerID: any = false;
  scheduledSlots = [];
  ScheduledSlots_1 = [];
  inProgressSlots = [];
  inProgressSlots_1 = [];
  completedSlots = [];
  completedSlots_1 = [];
  isValid: boolean = true;
  // fliterArray = [];
  // inProgressSlots_temp = [];
  dash_slots = "scheduled";
  shownGroup = null;
  stars = 4;
  shownList = null;
  checkList: any;
  groUid: any;
  checkRole: any;
  // bDetailsSubscripe: any;
  otpStatus = "notVerified";
  constructor(public navCtrl: NavController,
    public fetcher: FetcherServiceProvider,
    public navParams: NavParams,
    private afDB: AngularFireDatabase,
    // public fab: FabContainer,
    private afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    public events: Events,
    public http: Http,
    private locationAccuracy: LocationAccuracy,
    public splashScreen: SplashScreen,
    public diagnostic: Diagnostic) {
    this.events.subscribe('otpVerification', (status) => {
      if (status == true) {
        this.otpStatus = "verified";
      }
    });
    this.afAuth.authState.subscribe((auth) => {
      if (auth) {
        // this.fliterArray = [];
        this.groUid = auth.uid;
        this.groDetails(auth.uid);
      }
    })
  }

  ionViewDidEnter() {
    setTimeout(() => {
      if (this.splashScreen) {
        this.splashScreen.hide();
      }
    }, 6000);

  }
  sortFunction(a, b) {
    var dateA = moment(a.slotInfo.slotTimeLabel1).format("YYYY-MM-DD") + " " + a.slotInfo.slotTimeLabel2.toString().split(' ')[0]
    var dateB = moment(b.slotInfo.slotTimeLabel1).format("YYYY-MM-DD") + " " + b.slotInfo.slotTimeLabel2.toString().split(' ')[0]
    return +new Date(dateA) - +new Date(dateB);
  };

  groDetails(uid) {
    var vm = this;
    vm.groID = uid;
    var groBookingsInfoPath = "/staffDetails/gro/" + vm.groID + "/bookings";
    // firebase.database().ref(groBookingsInfoPath).orderByValue().on("value", function (data) {
    //   console.log("DATA_1" + data.val());
    // });
    vm.afDB.object(groBookingsInfoPath).valueChanges().subscribe(res => {
      // firebase.database().ref(groBookingsInfoPath).orderByValue().on("value", function (dateOne) {
      // var res = dateOne.val()
      if (res) {
        vm.scheduledSlots = [];
        vm.inProgressSlots = [];
        vm.completedSlots = [];
        vm.ScheduledSlots_1 = [];
        vm.inProgressSlots_1 = [];
        vm.completedSlots_1 = [];
        Object.keys(res).forEach(key => {
          var status = res[key]["status"];
          var bookingID = res[key]["bookingID"];
          var mode = res[key]["mode"];
          var bookingDetailsPath = "/bookingDetails/" + mode + "/" + bookingID;
          //console.log(bookingDetailsPath);
          if (status === "scheduled") {
            vm.afDB.object(bookingDetailsPath).valueChanges().subscribe(res2 => {
              // firebase.database().ref(bookingDetailsPath).orderByValue().on("value", function (dateTwo) {
              //var res2 = dateTwo.val();
              if (res2) {
                if (res2["addOnDetails"] && res2["addOnDetails"].length > 0) {
                  res2["addOnDetails"] = vm.convertToLowerCase(res2["addOnDetails"]);
                }
                res2["bookingID"] = bookingID;
                //console.log("Booking" + bookingID)
                var bookingObjIdx = vm.scheduledSlots.map(item => { return item["bookingID"]; }).indexOf(bookingID);
                if (bookingObjIdx >= 0) {
                  vm.scheduledSlots.splice(bookingObjIdx, 1)
                }
                vm.scheduledSlots.push(res2);
                vm.scheduledSlots.sort(vm.sortFunction);
                vm.ScheduledSlots_1 = vm.scheduledSlots;
              }
            })
          } else if (status === "started") {
            vm.afDB.object(bookingDetailsPath).valueChanges().subscribe(res3 => {
              //firebase.database().ref(bookingDetailsPath).orderByValue().on("value", function (dateThree) {
              //var res3 = dateThree.val();
              //console.log(res3);
              res3["bookingID"] = bookingID;
              if (res3["serviceList"]) {
                vm.checkList = res3["serviceList"];
              }
              var bookingObjIdx = vm.inProgressSlots.map(item => { return item["bookingID"]; }).indexOf(bookingID);
              if (bookingObjIdx >= 0) {
                vm.inProgressSlots.splice(bookingObjIdx, 1)
              }
              vm.inProgressSlots.push(res3);
              console.log(vm.inProgressSlots);
              // vm.inProgressSlots.sort(vm.sortFunction);​
              vm.inProgressSlots_1 = vm.inProgressSlots;
            })
          } else if (status === "completed") {
            vm.afDB.object(bookingDetailsPath).valueChanges().subscribe(res4 => {
              // firebase.database().ref(bookingDetailsPath).orderByValue().on("value", function (dateFour) {
              // var res4 = dateFour.val();
              //console.log(res4);
              res4["bookingID"] = bookingID;
              var bookingObjIdx = vm.completedSlots.map(item => { return item["bookingID"]; }).indexOf(bookingID);
              if (bookingObjIdx >= 0) {
                vm.completedSlots.splice(bookingObjIdx, 1)
              }
              vm.completedSlots.push(res4);
              vm.completedSlots.sort(vm.sortFunction);
              vm.completedSlots_1 = vm.completedSlots;
            })
            //console.log(this.completedSlots);
          }
        })

      }
    })
  }
  convertToLowerCase(addOnArray) {
    var a;
    var newArray = []
    for (a of addOnArray) {
      newArray.push(a.toLowerCase());
    }
    return newArray;
  }

  pauseService(bookingItem) {
    var bookingID = bookingItem.bookingID;
    var groName = bookingItem.slotInfo.groInfo.name;
    var groID = bookingItem.slotInfo.groInfo.uid;
    var mode = bookingItem.service;
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: true,
      cssClass: "pauseService"
    };

    const myModal: Modal = this.modalCtrl.create(
      PauseModalPage,
      { "bookingID": bookingID, "groName": groName, "groID": groID, "mode": mode },
      myModalOptions
    );

    myModal.present();

    myModal.onDidDismiss((data) => {
      console.log("I have dismissed.");
      console.log(data);
      console.log(this.otpStatus)
    });

    myModal.onWillDismiss((data) => {
      console.log("I'm about to dismiss");
      console.log(data);
    });

  }

  toChange(e) {
    //this.groDetails(this.groUid);
    if (e._value == 'scheduled') {
      this.scheduledSlots = [];
      this.scheduledSlots = this.ScheduledSlots_1;
    }
    else if (e._value == 'inProgress') {
      this.inProgressSlots = [];
      this.inProgressSlots = this.inProgressSlots_1;
    }
    else {
      this.completedSlots = [];
      this.completedSlots = this.completedSlots_1;
    }
  }



  resumeService(bookingItem) {
    const confirm = this.alertCtrl.create({
      title: 'Alert?',
      message: 'Are you sure want to Service Resume?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            var bookingID = bookingItem.bookingID;
            var groName = bookingItem.slotInfo.groInfo.name;
            var groID = bookingItem.slotInfo.groInfo.uid;
            var mode = bookingItem.service;
            var grlDetailsPath = "/staffDetails/grl/";
            // var bookingDetailsPath = "/bookingDetails/"+mode+"/"+bookingID;
            var adminEmail = "mng.ghial@gmrgroup.in";
            var commsObj = { "mType": "resumeService", "bookingID": bookingID, "groName": groName };
            var commsUrl = "https://us-central1-applied-craft-217711.cloudfunctions.net/hialms";
            var pausedPath = "/bookingDetails/" + mode + "/" + bookingID + "/pauseInfo/";
            var payload = {
              "paused": false,
              "resumeAt": new Date().getTime()
            }
            this.afDB.object(pausedPath).update(payload);
            // // Comment out the below block from text "start" to "end" before sending to client
            // // Start
            this.afDB.object(grlDetailsPath).valueChanges().subscribe(res => {
              var omitList = ["gmrprimegrl@hial.com", "raj@gmail.com", "grl@hial.in", "logesh@gmrprime.in"];
              var emailList = [adminEmail];
              var mobileList = [];
              Object.keys(res).forEach(key => {
                var email = res[key]["email"];
                var mobile = res[key]["mobileNo"];
                if ((res[key]["isActivated"]) && (omitList.indexOf(email) != -1)) {
                  emailList.push(email);
                  mobileList.push(mobile);
                }
              })
              commsObj["emailList"] = emailList;
              commsObj["mobileList"] = mobileList;
              console.log(commsObj)
              var headers = new Headers();
              headers.append('Content-Type', 'application/json');
              this.http.post(commsUrl, JSON.stringify(commsObj), { headers: headers })
                .pipe(map(res2 => res2.json()))
                .subscribe(data => {
                  console.log(data);
                });
            })

            // // End

            // Comment code from text "start" to "end" before taking a build and pushing the apk to cloud storage
            // // Start

            // commsObj["emailList"] = ["stalin@newgendigital.com"];
            // commsObj["mobileList"] = ["8939484295"];
            // console.log(commsObj)
            // var headers = new Headers();
            // headers.append('Content-Type', 'application/json');
            // this.http.post(commsUrl, JSON.stringify(commsObj), { headers: headers })
            //   .pipe(map(res2 => res2.json()))
            //   .subscribe(data => {
            //     console.log(data);
            //   });

            // // End
          }
        }
      ]
    });
    confirm.present();
  }



  ionViewDidLoad() {
    console.log(this.scheduledSlots);
    console.log('ionViewDidLoad DashGroPage');
    this.events.subscribe('role', role => {
      this.checkRole = role;
    })
    //console.log(this.checkRole)
  }

  verifyOTP(bookingInfo) {
    if (this.inProgressSlots.length == 0 || (this.inProgressSlots[0].pauseInfo && this.inProgressSlots[0].pauseInfo.paused)) {
      const myModalOptions: ModalOptions = {
        enableBackdropDismiss: true
      };
      const myModal: Modal = this.modalCtrl.create(PopStartServicePage, { "serviceOTP": bookingInfo.serviceOTP }, myModalOptions);
      myModal.present();
      myModal.onDidDismiss((data) => {
        console.log("I have dismissed.");
        console.log(data);
        console.log(this.otpStatus)
        if (this.otpStatus === "verified") {
          this.primeService(bookingInfo);
        }
      });
      myModal.onWillDismiss((data) => {
        console.log("I'm about to dismiss");
        console.log(data);
      });
    }
    else {
      const alert = this.alertCtrl.create({
        title: 'Alert!',
        subTitle: 'Oops,You have service inProgress so could not take another Services!',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  // trackGuest(mode, bookingID, guestFirstName, guestLastName) {
  //   console.log(bookingID);
  //   var trackingPath = "geoTracking/" + bookingID;
  //   this.afDB.object(trackingPath).valueChanges().subscribe(res => {
  //     if (!res && bookingID != "ekg6a51546147209382") {
  //       let alert = this.alertCtrl.create({
  //         title: 'The Guest Is Yet To Enable Tracking',
  //         subTitle: 'Please Try Again',
  //         buttons: ['Dismiss']
  //       });
  //       alert.present();
  //     } else {
  //       this.navCtrl.push(TrackGuestPage, { "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName });
  //     }
  //   })
  // }

  trackGuest(status, serviceList, bookingID, guestFirstName, guestLastName, guestPhone) {
    var trackingPath = "geoTracking/" + bookingID;
    // console.log(trackingPath);
    // console.log("GV65YU1549792347457");
    this.afDB.object(trackingPath).query.once("value").then(res => {
      console.log(res.val());
      if (res.val() == null) {
        let alert = this.alertCtrl.create({
          title: 'The Guest Is Yet To Enable Tracking',
          subTitle: 'Please Try Later',
          buttons: ['Dismiss']
        });
        alert.present();
      } else {
        if (res["status"] === "reached") {
          let alert = this.alertCtrl.create({
            title: 'The Guest Has Reached The Venue',
            subTitle: 'Please Try Later',
            buttons: ['Dismiss']
          });
          alert.present();
        }
        if (this.diagnostic.isGpsLocationEnabled()) {
          this.diagnostic.isGpsLocationEnabled().then((enabled) => {
            console.log('Gps enabled' + enabled);
            if (enabled) {
              this.navCtrl.push(TrackmapPage, { "bookingStatus": status, "serviceList": serviceList, "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName, "guestPhone": guestPhone });

            }
            else {
              this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                console.log('sdfdrjfg' + canRequest);
                if (canRequest) {
                  this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
                    this.navCtrl.push(TrackmapPage, { "bookingStatus": status, "serviceList": serviceList, "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName, "guestPhone": guestPhone });

                  }).catch((error) => {
                    const alert = this.alertCtrl.create({
                      title: 'Alert!',
                      message: 'You have disabled GPS',
                      buttons: [{
                        text: 'Ok',
                        handler: () => {

                        }
                      }]
                    });
                    alert.present()
                  });
                }
              });
            }

          },
            (error) => {


              console.error("The following error occurred: " + error);
            });
        }
        else {
          this.diagnostic.isLocationEnabled().then((enabled) => {
            if (enabled) {
              this.navCtrl.push(TrackmapPage, { "bookingStatus": status, "serviceList": serviceList, "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName, "guestPhone": guestPhone });


            }
            else {
              this.locationAccuracy.canRequest().then((canRequest: boolean) => {
                if (canRequest) {
                  this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
                    this.navCtrl.push(TrackmapPage, { "bookingStatus": status, "serviceList": serviceList, "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName, "guestPhone": guestPhone });

                  }).catch((error) => {
                    const alert = this.alertCtrl.create({
                      title: 'Alert!',
                      message: 'You have disabled GPS',
                      buttons: [{
                        text: 'Ok',
                        handler: () => {

                        }
                      }]
                    });
                    alert.present()
                  });
                }
              });
            }
          }, (error) => {


          });
        }






        // this.navCtrl.push(TrackmapPage, { "bookingStatus": status, "serviceList": serviceList, "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName, "guestPhone": guestPhone });


        // console.log(bookingID);
        // this.navCtrl.push(TrackGuestPage, { "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName });
        // this.navCtrl.push(TrackmapPage, { "bookingStatus": status, "serviceList": serviceList, "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName, "guestPhone": guestPhone });
      }
    })

  }

  showPlacard(name, msg) {
    this.navCtrl.push(PlacardPage, { "plaCardName": name });
  }

  primeService(bookingInfo) {
    let alert = this.alertCtrl.create({
      title: 'Start Service',
      enableBackdropDismiss: false,
      subTitle: 'OTP verified successfully. You Sure Want To Start The Service?',
      buttons: [
        {
          text: 'YES',
          role: 'Dismiss',
          handler: () => {
            var bookingID = bookingInfo["bookingID"];
            var mode = bookingInfo["service"];
            var guestID = bookingInfo["uid"];
            var update = {};
            // GRO Will click start service only after receiving the guest and verifying otp, hence we will set the value to true in the beginning

            var items = [
              { "title": "RECEIVED GUEST", "checked": "true", "key": "guestReceived" },
              { "title": "OTP VERIFIED", "checked": "true", "key": "otpVerified" }
            ];
            if (mode === "departure") {
              if (bookingInfo["addOnDetails"]) {
                if (bookingInfo["addOnDetails"].length === 2) {
                  items.push({ "title": "PORTER", "checked": "false", "key": "porter" });
                  items.push({ "title": "CHECK IN", "checked": "false", "key": "checkIn" });
                  items.push({ "title": "SECURITY", "checked": "false", "key": "security" });
                  var sector = bookingInfo["flInfo"]["mode"];
                  if (sector === "international") {
                    items.push({ "title": "IMMIGRATION", "checked": "false", "key": "immigration" });
                  }
                  items.push({ "title": "LOUNGE", "checked": "false", "key": "lounge" });
                }
                else {
                  if (bookingInfo["addOnDetails"][0].toLowerCase() === "porter service") {
                    items.push({ "title": "PORTER", "checked": "false", "key": "porter" });
                  }
                  items.push({ "title": "CHECK IN", "checked": "false", "key": "checkIn" });
                  items.push({ "title": "SECURITY", "checked": "false", "key": "security" });
                  var sector = bookingInfo["flInfo"]["mode"];
                  if (sector === "international") {
                    items.push({ "title": "IMMIGRATION", "checked": "false", "key": "immigration" });
                  }
                  if (bookingInfo["addOnDetails"][0].toLowerCase() === "lounge") {
                    items.push({ "title": "LOUNGE", "checked": "false", "key": "lounge" });
                  }
                }
              }
              else if (bookingInfo["pkgInfo"]["pkgName"].toLowerCase() === 'platinum package') {
                items.push({ "title": "PORTER", "checked": "false", "key": "porter" });
                items.push({ "title": "CHECK IN", "checked": "false", "key": "checkIn" });
                items.push({ "title": "SECURITY", "checked": "false", "key": "security" });
                var sector = bookingInfo["flInfo"]["mode"];
                if (sector === "international") {
                  items.push({ "title": "IMMIGRATION", "checked": "false", "key": "immigration" });
                }
                items.push({ "title": "LOUNGE", "checked": "false", "key": "lounge" });
              }
              else {
                items.push({ "title": "CHECK IN", "checked": "false", "key": "checkIn" });
                items.push({ "title": "SECURITY", "checked": "false", "key": "security" });
                var sector = bookingInfo["flInfo"]["mode"];
                if (sector === "international") {
                  items.push({ "title": "IMMIGRATION", "checked": "false", "key": "immigration" });
                }
              }
            }
            else if (mode === "arrival") {
              if (bookingInfo["addOnDetails"]) {
                if (bookingInfo["addOnDetails"][0].toLowerCase() === "porter service") {
                  items.push({ "title": "PORTER", "checked": "false", "key": "porter" });
                }
              }
              var sector = bookingInfo["flInfo"]["mode"];
              if (sector === "international") {
                items.push({ "title": "IMMIGRATION", "checked": "false", "key": "immigration" });
              }
            } else { //Transfer
              if (bookingInfo["addOnDetails"]) {
                if (bookingInfo["addOnDetails"].length === 2) {
                  items.push({ "title": "PORTER", "checked": "false", "key": "porter" });
                  items.push({ "title": "CHECK IN", "checked": "false", "key": "checkIn" });
                  items.push({ "title": "SECURITY", "checked": "false", "key": "security" });
                  var sector = bookingInfo["flInfo"]["mode"];
                  if (sector === "international") {
                    items.push({ "title": "IMMIGRATION", "checked": "false", "key": "immigration" });
                  }
                  items.push({ "title": "LOUNGE", "checked": "false", "key": "lounge" });
                }
                else {
                  if (bookingInfo["addOnDetails"][0].toLowerCase() === "porter service") {
                    items.push({ "title": "PORTER", "checked": "false", "key": "porter" });
                  }
                  items.push({ "title": "CHECK IN", "checked": "false", "key": "checkIn" });
                  items.push({ "title": "SECURITY", "checked": "false", "key": "security" });
                  var sector = bookingInfo["flInfo"]["departureInfo"]["mode"];
                  if (sector.indexOf("i") >= 0) {
                    items.push({ "title": "IMMIGRATION", "checked": "false", "key": "immigration" });
                  }
                  if (bookingInfo["addOnDetails"][0].toLowerCase() === "lounge") {
                    items.push({ "title": "LOUNGE", "checked": "false", "key": "lounge" });
                  }
                }
              }
              else if (bookingInfo["pkgInfo"]["pkgName"].toLowerCase() === 'platinum package') {
                items.push({ "title": "PORTER", "checked": "false", "key": "porter" });
                items.push({ "title": "CHECK IN", "checked": "false", "key": "checkIn" });
                items.push({ "title": "SECURITY", "checked": "false", "key": "security" });
                var sector = bookingInfo["flInfo"]["mode"];
                if (sector === "international") {
                  items.push({ "title": "IMMIGRATION", "checked": "false", "key": "immigration" });
                }
                items.push({ "title": "LOUNGE", "checked": "false", "key": "lounge" });
              }
              else {
                items.push({ "title": "CHECK IN", "checked": "false", "key": "checkIn" });
                items.push({ "title": "SECURITY", "checked": "false", "key": "security" });
                var sector = bookingInfo["flInfo"]["departureInfo"]["mode"];
                if (sector.indexOf("i") >= 0) {
                  items.push({ "title": "IMMIGRATION", "checked": "false", "key": "immigration" });
                }
              }
            }
            items.push({ "title": "RATING", "checked": "false", "key": "rating" });
            items.push({ "title": "SERVICE COMPLETED", "checked": "false", "key": "serviceCompleted" });

            items[0]["completedAt"] = moment.tz('Asia/Kolkata').toString();
            items[1]["completedAt"] = moment.tz('Asia/Kolkata').toString();

            this.checkList = items;

            //console.log("checkList:" + this.checkList);

            // When a service is started, update the status in all below paths
            // geoTracking/[bookingID]/status/service
            // bookingDetails/[arrival/departure/transfer]/bookingID
            // staffDetails/gro/[groID]/bookings/[bookingID]/status
            // userDetails/[guestID]/booking/[bookingID]/status


            var geoTrackingPath = "/geoTracking/" + bookingID + "/status/service";
            var bookingDetailsStatusPath = "/bookingDetails/" + mode + "/" + bookingID + "/status";
            var groBookingPath = "/staffDetails/gro/" + this.groID + "/bookings/" + bookingID + "/status";
            var guestBookingPath = "/userDetails/" + guestID + "/booking/" + bookingID + "/status";
            var bookingDetailsServiceListPath = "/bookingDetails/" + mode + "/" + bookingID + "/serviceList";

            update[geoTrackingPath] = "started";
            update[bookingDetailsStatusPath] = "started";
            update[groBookingPath] = "started";
            update[guestBookingPath] = "started";
            update[bookingDetailsServiceListPath] = items;
            console.log(update);
            this.afDB.object('/')
              .update(JSON.parse(JSON.stringify(update)))
              .then(res => {
                var bookingObjIdx = this.scheduledSlots.map(item => { return item["bookingID"]; }).indexOf(bookingID);
                console.log(bookingObjIdx);
                if (bookingObjIdx >= 0) {
                  this.scheduledSlots.splice(bookingObjIdx, 1);
                }
              })
            this.otpStatus = "notVerified";
            this.dash_slots = "inProgress";
            var pushTxt = "OTP has been verified successfully. Welcome to RGIA";
            this.pushStatus(pushTxt, guestID);

            // this.navCtrl.push(PrimePage, { "bookingInfo": bookingInfo });
          }
        },
        {
          text: 'NO',
          role: 'Dismiss',
          handler: () => {
            this.otpStatus = "notVerified";
          }
        },
      ]
    });
    alert.present();

    // console.log(bookingInfo)

  }

  serviceChkList(bookingInfo) {
    this.navCtrl.push(PrimePage, { "bookingInfo": bookingInfo });
  }

  updateStatus(index, idx, key, title, bookingID, mode, guestID, item) {

    var vm = this;
    //Validate Condition #1
    if (item.pauseInfo && item.pauseInfo.paused) {
      let alert = this.alertCtrl.create({
        title: 'You can not take ' + title + ',since your service in paused',
        buttons: ['Dismiss']
      })
      vm.inProgressSlots[index]["serviceList"][idx]["checked"] = false;
      alert.present();
      return;
    }
    //Validate Condition #2
    console.log(idx);
    for (var i = 0; i < idx; i++) {
      console.log(item.serviceList[i]["checked"]);
      if (!item.serviceList[i]["checked"] || item.serviceList[i]["checked"] == "false") {
        let alert = this.alertCtrl.create({
          title: 'Please check the service check list in orderly manner.',
          buttons: [{
            text: 'Dismiss',
            role: 'Dismiss',
            handler: () => {
              vm.inProgressSlots[index]["serviceList"][idx]["checked"] = false;
            }
          }]
        })
        alert.present();
        return;
      }
    }
    if (key === "serviceCompleted") {
      let alert = vm.alertCtrl.create({
        title: 'Completed ' + title,
        enableBackdropDismiss: false,
        subTitle: 'Confirm Action',
        buttons: [
          {
            text: 'YES',
            role: 'Dismiss',
            handler: () => {
              var d = moment.tz('Asia/Kolkata').toString();
              var serviceItemStatusPath = "/bookingDetails/" + mode + "/" + bookingID + "/serviceList/" + idx + "/checked";
              var serviceItemCompletedAtPath = "/bookingDetails/" + mode + "/" + bookingID + "/serviceList/" + idx + "/completedAt";
              var bookingDetailsStatusPath = "/bookingDetails/" + mode + "/" + bookingID + "/status";
              var groBookingStatusPath = "/staffDetails/gro/" + vm.groID + "/bookings/" + bookingID + "/status";
              var guestBookingStatusPath = "/userDetails/" + guestID + "/booking/" + bookingID + "/status";
              var update = {};
              update[serviceItemStatusPath] = "true";
              update[serviceItemCompletedAtPath] = d;
              update[bookingDetailsStatusPath] = "completed";
              update[groBookingStatusPath] = "completed";
              update[guestBookingStatusPath] = "completed";
              vm.afDB.object("/").update(update);
              vm.dash_slots = "completed";
              var pushTxt = "Your booking has been served, please rate us";
              //vm.pushStatus(pushTxt, guestID);
            }
          },
          {
            text: 'NO',
            role: 'Dismiss',
            handler: () => {
              vm.inProgressSlots[index]["serviceList"][idx]["checked"] = false;
              // console.log(vm.checkList[idx])
              // vm.checkList[idx]["checked"] = "false";
            }
          }
        ]
      })
      alert.present();
    } else if (key === "rating") {
      let alert = vm.alertCtrl.create({
        title: 'Rate The Guest',
        enableBackdropDismiss: false,
        subTitle: 'Confirm Action',
        buttons: [
          {
            text: 'YES',
            role: 'Dismiss',
            handler: () => {
              const myModalOptions: ModalOptions = {
                enableBackdropDismiss: false,
              };
              const myModal: Modal = vm.modalCtrl.create(RatingServicePage, { "mode": mode, "bookingID": bookingID, "idx": idx }, myModalOptions);
              myModal.present();
              myModal.onDidDismiss((data) => {
                // console.log("I have dismissed.");
                // console.log(data);
                if (data == undefined) {
                  vm.inProgressSlots[index]["serviceList"][idx]["checked"] = false;
                }
              });
              myModal.onWillDismiss((data) => {
                // console.log("I'm about to dismiss");
                // console.log(data);
                if (data == undefined) {
                  vm.inProgressSlots[index]["serviceList"][idx]["checked"] = false;
                }
              });
            }
          },
          {
            text: 'NO',
            role: 'Dismiss',
            handler: () => {
              vm.inProgressSlots[index]["serviceList"][idx]["checked"] = false;
              // console.log(vm.inProgressSlots[index]["serviceList"][idx]["checked"]);
              // vm.inProgressSlots[index]["serviceList"][idx]["checked"] = "false";
            }
          }
        ]
      })
      alert.present();
    } else {
      let alert = vm.alertCtrl.create({
        title: 'Completed ' + title,
        enableBackdropDismiss: false,
        subTitle: 'Confirm Action',
        buttons: [
          {
            text: 'YES',
            role: 'Dismiss',
            handler: () => {
              var d = moment.tz('Asia/Kolkata').toString();
              var serviceItemStatusPath = "/bookingDetails/" + mode + "/" + bookingID + "/serviceList/" + idx + "/checked";
              var serviceItemCompletedAtPath = "/bookingDetails/" + mode + "/" + bookingID + "/serviceList/" + idx + "/completedAt";
              var update = {};
              update[serviceItemStatusPath] = "true";
              update[serviceItemCompletedAtPath] = d;
              firebase.database().ref().update(update);
              //vm.afDB.object("/").update(update);
              // var pushTxt = "Our GRO has completed "+title;
              // vm.pushStatus(pushTxt,guestID);
            }
          },
          {
            text: 'NO',
            role: 'Dismiss',
            handler: () => {
              // var bookingDetailsPath = "/bookingDetails/" + mode + "/" + bookingID;
              // firebase.database().ref(bookingDetailsPath).orderByValue().on("value", function (dateThree) {
              //   var res3 = dateThree.val();
              //   //console.log(res3);
              //   res3["bookingID"] = bookingID;
              //   if (res3["serviceList"]) {
              //     vm.checkList = res3["serviceList"];
              //   }
              //   var bookingObjIdx = vm.inProgressSlots.map(item => { return item["bookingID"]; }).indexOf(bookingID);
              //   if (bookingObjIdx >= 0) {
              //     vm.inProgressSlots.splice(bookingObjIdx, 1)
              //   }
              //   vm.inProgressSlots.push(res3);
              // })
              vm.inProgressSlots[index]["serviceList"][idx]["checked"] = false;
            }
          }
        ]
      })
      alert.present();
    }
  }

  autoComplete(slotDate, slotTime, serviceMode, bookingID, userID) {
    var vm = this;
    var _disabled = true;
    var slotTimeFormat = slotDate + " " + slotTime.replace(" hrs (IST)", "");
    var slotDateTime = new Date(slotTimeFormat);
    var todayDateTime = new Date();
    // console.log(slotDateTime);
    // console.log(todayDateTime);
    if (slotDateTime >= todayDateTime) {
      _disabled = false;
    }
    else {
      _disabled = true;
      //autoComplted
      var bookingDetailsStatusPath = "/bookingDetails/" + serviceMode.toLowerCase() + "/" + bookingID + "/status";
      var groBookingStatusPath = "/staffDetails/gro/" + vm.groID + "/bookings/" + bookingID + "/status";
      var guestBookingStatusPath = "/userDetails/" + userID + "/booking/" + bookingID + "/status";
      var autoCompletePath = "/bookingDetails/" + serviceMode.toLowerCase() + "/" + bookingID + "/autoCompleted";
      var update = {};
      update[bookingDetailsStatusPath] = "completed";
      update[groBookingStatusPath] = "completed";
      update[guestBookingStatusPath] = "completed";
      update[autoCompletePath] = true;
      //console.log(update);
      vm.afDB.object('/').update(update);
    }
    return _disabled;
  }

  pushStatus(pushTxt, guestID) {
    var vm = this;
    var oneSignalRef = firebase.database().ref("/userDetails/" + guestID + "/oneSignal");
    oneSignalRef.orderByValue().on("value", function (res) {
      if (res) {
        var playerID = res["playerId"];
        var pushObj = {
          "app_id": "160d50df-cfce-4b81-b30b-4a052c7609c0",
          "contents": { "en": pushTxt },
          "include_player_ids": [playerID]
        }
        console.log(pushObj);
        vm.fetcher.pushNotification(pushObj).then(resp => {
        })
      }
    });
    // this.http.post(this.oneSignalUrl, pushObj, { headers: headers })
    //   .pipe(map(res2 => res2.json()))
    //   .subscribe(data => {
    //     console.log(data);
    //   });
    //   }
    // })
  }

  getscheduledSlots(ev: any) {
    var vm = this;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.scheduledSlots = this.scheduledSlots.filter((item) => {
        return (item.bookingID.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.guestInfo.guestInfo[0].firstName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.scheduledSlots = [];
      this.scheduledSlots = this.ScheduledSlots_1;
    }
  }

  getinProgressSlots(ev: any) {
    var vm = this;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.inProgressSlots = this.inProgressSlots.filter((item) => {
        return (item.bookingID.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.guestInfo.guestInfo[0].firstName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.inProgressSlots = [];
      this.inProgressSlots = this.inProgressSlots_1;
    }
  }

  getcompletedSlots(ev: any) {
    var vm = this;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.completedSlots = this.completedSlots.filter((item) => {
        return (item.bookingID.toLowerCase().indexOf(val.toLowerCase()) > -1 || item.guestInfo.guestInfo[0].firstName.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
    else {
      this.completedSlots = [];
      this.completedSlots = this.completedSlots_1;
    }
  }

  guestCount(a, c, i) {
    var totalGuest = 0;
    totalGuest = parseInt(a) + parseInt(c) + parseInt(i);
    return totalGuest;
  }


  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };

  isGroupShown(group) {
    return this.shownGroup === group;
  };

  toggleList(group) {
    if (this.isListShown(group)) {
      this.shownList = null;
    } else {
      this.shownList = group;
    }
  };

  isListShown(group) {
    return this.shownList === group;
  };

  fliterService(service, status) {
    // console.log(service);
    // console.log(status);
    var tempArray = [];
    if (status == 'scheduled') {

      if (service == 'all') {
        this.scheduledSlots = this.ScheduledSlots_1;
      }
      else {
        this.scheduledSlots = [];
        this.scheduledSlots = this.ScheduledSlots_1;
        for (let index = 0; index < this.scheduledSlots.length; index++) {
          if (this.scheduledSlots[index].service.toLowerCase() == service.toLowerCase()) {
            tempArray.push(this.scheduledSlots[index])
          }
        }
        this.scheduledSlots = tempArray;
      }
    }
    else if (status == 'completed') {
      if (service == 'all') {
        this.completedSlots = this.completedSlots_1;
      }
      else {
        this.completedSlots = [];
        this.completedSlots = this.completedSlots_1;
        for (let index = 0; index < this.completedSlots.length; index++) {
          if (this.completedSlots[index].service.toLowerCase() == service.toLowerCase()) {
            tempArray.push(this.completedSlots[index])
          }
        }
        this.completedSlots = tempArray;
      }
    }
    else { //inProgress
    }
  }
}
