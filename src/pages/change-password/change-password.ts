import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
// import { DashboardGroPage } from '../dashboard-gro/dashboard-gro';
import { DashGrlPage } from '../dash-grl/dash-grl';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase } from 'angularfire2/database';
/**
 * Generated class for the ChangePasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
})
export class ChangePasswordPage {
  changePwdForm: FormGroup;
  error: string;
  roleType: string;
  // userObj:any;
  constructor(public events: Events,public db: AngularFireDatabase, public afAuth: AngularFireAuth, public spinner: NgxSpinnerService, public navCtrl: NavController, public navParams: NavParams, public fb: FormBuilder) {
    // this.userObj =  this.navParams.get('userObj');
    this.roleType = navParams.get("roleType");
    this.changePwdForm = fb.group({
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }
  changePwd() {
    let data = this.changePwdForm.value;
    if (!data.password) {
      return;
    }
    this.spinner.show();
    this.afAuth.auth.currentUser.updatePassword(data.password).then((val) => {
      // update defaultPwd flag
      console.log(this.afAuth.auth.currentUser.uid);
      var staffDetailPath = 'staffDetails/' + this.roleType +"/"+this.afAuth.auth.currentUser.uid;
      console.log(staffDetailPath);
      this.db.object(staffDetailPath).update({ 'isActivated': true }).then( (val) => {
        this.spinner.hide();
        this.navCtrl.setRoot(DashGrlPage)
      }).catch((error) => {
        this.spinner.hide();
        console.log(error.message);
        this.error = error.message;
      });

    }).catch((error) => {
      this.spinner.hide();
      console.log(error.message);
      this.error = error.message;
    });
  }

}
