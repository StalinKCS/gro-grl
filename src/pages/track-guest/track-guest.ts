import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Platform } from 'ionic-angular';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { GoogleMaps, GoogleMap, LatLng, GoogleMapsEvent } from '@ionic-native/google-maps';
import * as firebase from 'firebase';
import { AngularFireDatabase } from 'angularfire2/database';

/**
 * Generated class for the TrackGuestPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

declare var google;
// Refer below links
// https://www.ghadeer.io/ionic-3-google-maps/
// https://github.com/ghadeer-io/ionic-3-google-maps-example/blob/master/src/pages/home/home.ts
@Component({
  selector: 'page-track-guest',
  templateUrl: 'track-guest.html',
})
export class TrackGuestPage {
  bookingID: string;
  guestName: string;
  @ViewChild('map') mapElement: ElementRef;
  private map:GoogleMap;
  private location:LatLng;

  constructor(public geolocation: Geolocation,
    public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public afDB: AngularFireDatabase,
    private googleMaps: GoogleMaps) {
      this.bookingID = navParams.get("bookingID");
      this.guestName = navParams.get("guestName");
      var geoTrackingPath = "/geoTracking/"+this.bookingID;
      this.afDB.object(geoTrackingPath).valueChanges().subscribe(res => {
        this.location = new LatLng(res["latitude"],res["longitude"])
      })
  }

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      let element = this.mapElement.nativeElement;
      this.map = this.googleMaps.create(element);
  
      this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        let options = {
          target: this.location,
          zoom: 15
        };
  
        this.map.moveCamera(options);
        setTimeout(() => {this.addMarker()}, 2000)
      });
    });
  }

  addMarker() {
    this.map.addMarker({
      title: this.guestName,
      icon: 'blue',
      animation: 'DROP',
      position: {
        lat: this.location.lat,
        lng: this.location.lng
      }
    })
    .then(marker => {
      marker.showInfoWindow();
      // marker.on(GoogleMapsEvent.MAP_READY).subscribe(() => {
      //   // alert('Marker Clicked');
      //   console.log('MARKER')
      // });
    });
  }

}
