import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PlacardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-placard',
  templateUrl: 'placard.html',
})
export class PlacardPage {
  plaCardName:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.plaCardName = navParams.get("plaCardName");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlacardPage');
  }

}
