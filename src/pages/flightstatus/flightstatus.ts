import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FlightDetailPage } from '../flight-detail/flight-detail';
// import { BookingPage } from '../booking/booking';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { UtilsProvider } from '../../providers/utils/utils';
import { FetcherServiceProvider } from '../../providers/fetcher-service/fetcher-service';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as moment from 'moment-timezone';
/**
 * Generated class for the FlightstatusPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-flightstatus',
  templateUrl: 'flightstatus.html',
})
export class FlightstatusPage {
  chooseArrivDep: any;
  // flightDate: any;
  minDate: any;
  maxDate: any;
  dateofflight: any;
  flightStatus: FormGroup;
  filteredItems: any;
  flightList: any;
  travelsec: any;
  showList: boolean = false;
  flNo: any;
  searchResult: any;
  showDetails: boolean = false;
  constructor(public fetcher: FetcherServiceProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,

    public utils: UtilsProvider,
    private afDB: AngularFireDatabase) {
    this.chooseArrivDep = "arrival";
    this.travelsec = "domestic";
    this.flightStatus = formBuilder.group({
      flightNo: ['', Validators.compose([Validators.minLength(5), Validators.maxLength(10), Validators.required])],
      chooseArrivDep: ['', Validators.required],
      flightDate: ['', Validators.required],
      travelsec: ['', Validators.required]
    });
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FlightstatusPage');
    this.minDate = new Date().toISOString();
    this.getMaxDate();
  }
  getMaxDate() {
    // var vm = this;
    let loader = this.utils.makeLoader('Please wait...');
    loader.present();
    // let payload = {}
    // var body = JSON.stringify(payload);
    // console.log(body);
    this.afDB.object("/flStatus/").valueChanges().subscribe(res => {
      this.maxDate = new Date(res["until"]).toISOString();
      loader.dismiss();

    })
    // vm.fetcher.maxDate(body).then(resp => {
    //   vm.spinner.hide();
    //   if (resp["status"] == "OK") {
    //     this.maxDate = new Date(resp["result"]).toISOString();
    //     // console.log(this.maxDate);
    //   }
    //   else {
    //     this.maxDate = new Date().getFullYear();
    //     // console.log(this.maxDate);
    //     vm.utils.makeToast('We are sorry. Something went wrong');
    //   }
    // });
  }

  isEmpty(val) {
    return (val === false || val === '' || val === undefined || val == null || val.length <= 0) ? true : false;
  }

  fetchFlightsList(dateofflight) {
    // let loader = this.utils.makeLoader('Please wait');
    // loader.present();
    dateofflight = dateofflight.replace(/-/g, "");
    if (dateofflight) {
      var queryPath = '/flightInfo/' + this.chooseArrivDep + "/" + this.travelsec + "/" + dateofflight + "/flList";
      this.flightStatus.controls.flightNo.setValue('');
      this.afDB.object(queryPath).valueChanges().subscribe(res => {
        if (res) {
          // this.showList = true;
          this.flightList = res;
          return this.flightList;
        }

      })
    } else {
      this.flightList = [];
      return this.flightList;
    }
    setTimeout(() => {
      
      this.showList = false;
  //     loader.dismiss();
  },500);
  }

  filterItem(value) {
    var cleanStr = value.replace(/[^\w\s]/gi, '');
    if (cleanStr) {
      this.showList = true;
      this.filteredItems = Object.assign([], this.flightList).filter(
        item => item.toLowerCase().split(' ').join('').indexOf(cleanStr.toLowerCase().split(' ').join('')) > -1
      )
    } else {
      this.showList = false;
    }
  }

  enterFlight(f) {
    this.flightStatus.controls.flightNo.setValue(f);
    this.showList = false;

  }

  clearvalues()
  {
    this.showList = false;

  }

  
  hasWhiteSpace(s) {
    // Check for white space
    if (/\s/g.test(s)) {
      //alert("Please Check Your Fields For Spaces");
      return true;
    }
    return false;
  }

  flightDetails() {
    if (this.isEmpty(this.flightStatus.controls.flightDate.value)) {
      this.utils.makeToast('Please enter flight date');
    }
    else if (this.isEmpty(this.flightStatus.controls.flightNo.value)) {
      this.utils.makeToast('Please enter flight no');
    }
    else {
      this.showList = false;
      var flNo = "";
      var flightNumber = this.hasWhiteSpace(this.flightStatus.controls.flightNo.value);
      if(flightNumber)
      {
        flNo = this.flightStatus.controls.flightNo.value;
      }
      else // Example 6E539 convert into 6E 539
      {
        flNo = (this.flightStatus.controls.flightNo.value.substring(0,2) +" "+ this.flightStatus.controls.flightNo.value.substring(2,this.flightStatus.controls.flightNo.value.length))
      }
      // console.log(this.flightStatus.controls.flightNo.value);
      console.log(flNo);
      var flDate = this.dateofflight.replace(/-/g, '');
      var flQueryPath = '/flightInfo/' + this.chooseArrivDep + "/" + this.travelsec + "/" + flDate + "/" + flNo.toUpperCase();
      console.log(flQueryPath);
      this.afDB.object(flQueryPath).valueChanges().subscribe(res => {
        console.log(res);
        if (res) {
          this.showDetails = true;
          console.log(res)
          res["m"] = this.chooseArrivDep;
          // this.navCtrl.push(FlightDetailPage, { result: res })

          this.searchResult = res;
          console.log(this.searchResult);
          //  Optimization Reminder: Refactor appropriately
          let loader = this.utils.makeLoader('Please wait...');
          loader.present();
          if (this.searchResult["m"] === "arrival") {
            var arrStatusCodes = {"ARR":"ARRIVED", "ASK":"ASK", "CNL":"CANCELLED", "DEL":"DELAY", "DIV":"DIVERTED", "ETA":"EXPECTED", "FBA":"FIRST BAG", "LBA":"LAST BAG", "NWS":"NEXT INFO", "NOP":"NON OPERATION", "RER":"RE ROUTED", "":"EXPECTED"};
            this.searchResult["flStatus"] = arrStatusCodes[this.searchResult["status"]];
            var sch = this.searchResult["stoa"];
            //  20181121001000
            // 2018-11-21T00:10:00.000Z
            sch = sch.substring(0, 4) + "-" + sch.substring(4, 6) + "-" + sch.substring(6, 8) + "T" + sch.substring(8, 10) + ":" + sch.substring(10, 12) + ":" + sch.substring(12, 14) + ".000Z";
            this.searchResult["sch"] = moment.utc(sch).format("MMM DD YYYY [at] HH:mm A");
            if (this.searchResult["eta"] === "") {
              this.searchResult["eta"] = this.searchResult["sch"];
            } else {
              var eta = this.searchResult["eta"];
              eta = eta.substring(0, 4) + "-" + eta.substring(4, 6) + "-" + eta.substring(6, 8) + "T" + eta.substring(8, 10) + ":" + eta.substring(10, 12) + ":" + eta.substring(12, 14) + ".000Z";
              this.searchResult["eta"] = eta;
            }
          } else if (this.searchResult["m"] === "departure") {
            var depStatusCodes = {"AIB":"DEPARTED", "DIV":"DIVERTED", "NGT":"NEW GATE", "NWS":"NEXT INFO", "NOP":"NON OPERATION", "ETD":"ON TIME", "RER":"RE ROUTED", "RES":"RE SCHEDULED", "GTO":"GATE OPEN", "BRD":"BOARDING", "LCL":"FINAL CALL", "GCL":"GATE CLOSE", "DEL":"DELAY", "CNL":"CANCELLED", "HAJ":"HAJ TERMINA", "DEP":"DEPARTED", "":"ON TIME"}
            this.searchResult["flStatus"] = depStatusCodes[this.searchResult["status"]];
            var sch = this.searchResult["stod"];
            sch = sch.substring(0, 4) + "-" + sch.substring(4, 6) + "-" + sch.substring(6, 8) + "T" + sch.substring(8, 10) + ":" + sch.substring(10, 12) + ":" + sch.substring(12, 14) + ".000Z";
            this.searchResult["sch"] = moment.utc(sch).format("MMM DD YYYY [at] HH:mm A");
            if (this.searchResult["etd"] === "") {
              this.searchResult["eta"] = this.searchResult["sch"];
            } else {
              var eta = this.searchResult["etd"];
              eta = eta.substring(0, 4) + "-" + eta.substring(4, 6) + "-" + eta.substring(6, 8) + "T" + eta.substring(8, 10) + ":" + eta.substring(10, 12) + ":" + eta.substring(12, 14) + ".000Z";
              this.searchResult["eta"] = eta;
            }
      
          
          }
          loader.dismiss();



        }
        else {
          this.utils.makeToast('Please enter valid flight number');
        }

      })


      // var vm = this;
      // vm.spinner.show();
      // let payload = {
      //   "f": this.flightStatus.controls.flightNo.value,
      //   "m": this.chooseArrivDep == "arriv" ? "A" : "D",
      //   "d": this.dateFormatYYYYMMDD(this.flightStatus.controls.flightDate.value)
      // }
      // var body = JSON.stringify(payload);
      // console.log(body);
      // vm.fetcher.flStatus(body).then(resp => {
      //   console.log(resp["status"]);
      //   vm.spinner.hide();
      //   console.log(resp);
      //   if (resp["status"] == "OK") {
      //     //console.log(resp["result"]);
      //     this.navCtrl.push(FlightDetailPage, { result: resp["result"] });
      //   }
      //   else if (resp["status"] == "NA") {
      //     // alert("currently no details available");
      //     vm.utils.makeToast('Currently no details available');
      //   }
      //   else {
      //     vm.utils.makeToast('We are sorry. Something went wrong');
      //   }
      // });
    }
  }
  dateFormatYYYYMMDD = function convert(str) {
    var date = new Date(str),
      mnth = ("0" + (date.getMonth() + 1)).slice(-2),
      day = ("0" + date.getDate()).slice(-2);
    return [date.getFullYear(), mnth, day].join("");
  }
  // navigateBack() {
  //   this.navCtrl.setRoot(BookingPage);
  // }
  changedArriDep()
  {
    // let loader = this.utils.makeLoader('Please wait');
    // loader.present();
   
      this.travelsec = "domestic";
      this.flightStatus.controls.flightDate.setValue('');
      this.flightStatus.controls.flightNo.setValue('');
      setTimeout(() => {
        this.showList = false;
    //     loader.dismiss();
    },500);
    
  }
  changedSector()
  { 
    // let loader = this.utils.makeLoader('Please wait');
    // loader.present();
    this.flightStatus.controls.flightDate.setValue('');
    this.flightStatus.controls.flightNo.setValue('');
    setTimeout(() => {
      this.showList = false;
  //     loader.dismiss();
  },500)
  }

}