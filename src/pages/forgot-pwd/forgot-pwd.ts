import { MgLoginPage } from './../mg-login/mg-login';
import { Component } from '@angular/core';
import { NavController, NavParams,AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { AngularFireAuth } from 'angularfire2/auth';
/**
 * Generated class for the ForgotPwdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
@Component({
  selector: 'page-forgot-pwd',
  templateUrl: 'forgot-pwd.html',
})
export class ForgotPwdPage {
  forgotPwdForm: FormGroup;
  error: string;
  constructor(public alertCtrl: AlertController,public afAuth: AngularFireAuth,public spinner: NgxSpinnerService,public navCtrl: NavController, public navParams: NavParams,public fb: FormBuilder) {
    this.forgotPwdForm = fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgotPwdPage');
  }

  forgotPwd()
  {
    var vm = this;
    let data = vm.forgotPwdForm.value;
    if (!data.email) {
      return;
    }
    vm.spinner.show();
    vm.afAuth.auth.sendPasswordResetEmail(data.email).then(function (val) {
      var alert = vm.alertCtrl.create({
        title: 'Alert!',
        message: 'Please check your email & Reset your Password',
        buttons: [ 'OK' ]
      });
      vm.navCtrl.setRoot(MgLoginPage);
      alert.present();
      }).catch(function (error) {
      vm.spinner.hide();
      console.log(error.message);
      vm.error = error.message;
    });
  }

}
