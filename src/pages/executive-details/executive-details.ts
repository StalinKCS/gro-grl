import { Component, Directive, Output, EventEmitter, Input, SimpleChange } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, Modal, ModalOptions } from 'ionic-angular';
import { SlotStatusPage } from '../slot-status/slot-status';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { ReallocateModalPage } from '../reallocate-modal/reallocate-modal';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { TrackmapPage } from '../trackmap/trackmap';
import * as moment from 'moment-timezone';

/**
 * Generated class for the ExecutiveDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-executive-details',
  templateUrl: 'executive-details.html',
})
export class ExecutiveDetailsPage {
  grlID: string;
  slotsList = [];
  groList = [];
  rosterGRO: any;
  dash_overview = "gro";
  shownGroup = null;
  shownSlot = null;
  slots_overview = "scheduledSlots";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private afDB: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {
    this.afAuth.authState.subscribe((auth) => {

      if (auth) {
        this.grlID = auth.uid;
        var bookingDetailsPath = "/bookingDetails";
        var groListPath = "/staffDetails/gro";
        this.afDB.list(groListPath).valueChanges().subscribe(res => {
          this.groList = res;
          console.log(this.groList);
        })
        this.afDB.object(bookingDetailsPath).valueChanges().subscribe(res => {
          Object.keys(res).forEach(key => {

            if (key != "departurePrivilegePlan") {
              var mode = res[key];
              Object.keys(mode).forEach(key => {
                var status = mode[key]["status"];
                mode[key]["bookingID"] = key;
                this.slotsList.push(mode[key]);
                if ((mode[key]["addOnDetails"]) && (mode[key]["addOnDetails"].length > 0)) {
                  mode[key]["addOnDetails"] = this.convertToLowerCase(mode[key]["addOnDetails"]);
                }
              })

            }
            console.log(this.slotsList)
          })
        })
      }

    })
  }

  convertToLowerCase(addOnArray) {
    var a;
    var newArray = []
    for (a of addOnArray) {
      newArray.push(a.toLowerCase());
    }
    return newArray;
  }

  getGroDetails() {
    this.afDB.list('/roster/042019/').valueChanges().subscribe(res => {
      this.rosterGRO = res[1];
    });
  }

  getLength(groUid, status) {
    //console.log(groUid);
    // var length = 0;
    // for (let i = 0; i < this.slotsList.length; i++) {
    //   if(this.slotsList[i]["slotInfo"]["groInfo"])
    //   {
    //     if(this.slotsList[i]["slotInfo"]["groInfo"]["uid"] == groUid && this.slotsList[i]["status"].toLowerCase() == status.toLowerCase())
    //     {
    //       length = length + 1;
    //     }
    //   }
    // }
    return 0;
  }

  reAllocateSlot(item) {
    //console.log(item);
    var bookingID = item.bookingID;
    var groInfo = item.slotInfo.groInfo;
    var service = item.service;
    var groID = groInfo.uid;
    var groDetails = [];
    for (var i = 0; i < this.groList.length; i++) {
      if ((this.groList[i]["uid"] != groID) && (this.groList[i]["isActivated"])) {
        groDetails.push(this.groList[i])
      }
    }
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: true
    };

    const myModal: Modal = this.modalCtrl.create(
      ReallocateModalPage,
      {
        "groList": groDetails,
        "bookingID": bookingID,
        "service": service,
        "currentGRO": groInfo,
        "reAllocate": true
      },

      myModalOptions);

    myModal.present();
  }

  trackGuest(status, serviceList, bookingID, guestFirstName, guestLastName, guestPhone) {
    var trackingPath = "geoTracking/" + bookingID;
    // var trackingPath = "geoTracking/0vki9t1548260915257";
    this.afDB.object(trackingPath).query.once("value").then(res => {
      if ((!res)) {
        let alert = this.alertCtrl.create({
          title: 'The Guest Is Yet To Enable Tracking',
          subTitle: 'Please Try Later',
          buttons: ['Dismiss']
        });
        alert.present();
      } else {
        if (res["status"] === "reached") {
          let alert = this.alertCtrl.create({
            title: 'The Guest Has Reached The Venue',
            subTitle: 'Please Try Later',
            buttons: ['Dismiss']
          });
          alert.present();
        }
        //console.log(bookingID);
        // this.navCtrl.push(TrackGuestPage, { "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName });
        this.navCtrl.push(TrackmapPage, { "bookingStatus": status, "serviceList": serviceList, "bookingID": bookingID, "guestName": guestFirstName + " " + guestLastName, "guestPhone": guestPhone });
      }
    })

  }

  toCheck(loggedObj) {
    const today = moment.tz('Asia/Kolkata').format('YYYYMMDD');
    // console.log(today);
    let todayLogin = false;
    if (loggedObj[today]) {
      todayLogin = true;
    }
    return todayLogin;
  }

  // Getting the date on YYYYMMDD format
  _yyyymmdd(slotDate) {
    var x = new Date(slotDate);
    var y = x.getFullYear().toString();
    var m = (x.getMonth() + 1).toString();
    var d = x.getDate().toString();
    (d.length == 1) && (d = '0' + d);
    (m.length == 1) && (m = '0' + m);
    var yyyymmdd = y + m + d;
    return yyyymmdd;
  }

  guestCount(a, c, i) {
    var totalGuest = 0;
    totalGuest = parseInt(a) + parseInt(c) + parseInt(i);
    return totalGuest;
  }

  todayBooking(uid) {
    var toDayCount = 0;
    let todayDate = this._yyyymmdd(new Date());
    for (let i = 0; i < this.slotsList.length; i++) {
      if(this.slotsList[i].status == 'booked' &&  this.slotsList[i].slotInfo && this.slotsList[i].slotInfo.groInfo && this.slotsList[i].slotInfo.groInfo.uid == uid)
      {
        let slotDateTimeFormat = this.slotsList[i].slotInfo.slotTimeLabel1 + " " + this.slotsList[i].slotInfo.slotTimeLabel2.replace(" hrs (IST)", "");
        let slotDateTime = this._yyyymmdd(slotDateTimeFormat);
        if (todayDate == slotDateTime) {
          toDayCount = toDayCount + 1;
        }
      }
    }
    return toDayCount;
  }

  toDetect(uid) {
    let todaDate = new Date().getDate();
    if (this.rosterGRO && this.rosterGRO.hasOwnProperty(uid)) {
      return this.rosterGRO[uid].schedule[todaDate - 1].shift;
    }
    else {
      return 'None'
    }
  }

  ionViewDidLoad() {
    this.getGroDetails();
    console.log('ionViewDidLoad DashGrlPage');
  }
  gotonext() {
    this.navCtrl.push(SlotStatusPage);
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };

  toggleSlot(group) {
    if (this.isSlotShown(group)) {
      this.shownSlot = null;
    } else {
      this.shownSlot = group;
    }
  };

  isSlotShown(group) {
    return this.shownSlot === group;
  };

  slotCount(groID, slotType) {
    var slotTypeId = slotType + groID;
    console.log(slotTypeId);
  }

  slotChk(groID, slotType) {
    console.log(groID, slotType)
  }

}
