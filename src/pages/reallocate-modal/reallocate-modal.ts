import { Component } from '@angular/core';
import { NavController, NavParams, ViewController,AlertController  } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as moment from 'moment-timezone';
import { Http, RequestOptions, Request, RequestMethod, Headers } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { map } from 'rxjs/operators';

/**
 * Generated class for the ReallocateModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-reallocate-modal',
  templateUrl: 'reallocate-modal.html',
})
export class ReallocateModalPage {
  groList:any;
  bookingID:any;
  service:any;
  guestID:any;
  currentGRO:any;
  reAllocate:any;
  slotTime:any;
  player_id:any;
  guestMobNo:any;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController,
    public navParams: NavParams,
    private afDB: AngularFireDatabase, 
    public viewCtrl: ViewController,
    public http: Http) {
    this.groList = this.navParams.get("groList");
    this.bookingID = this.navParams.get("bookingID");
    this.service = this.navParams.get("service");

    this.slotTime = this.navParams.get("slotTime");
    this.player_id = this.navParams.get("player_id");
    this.guestMobNo = this.navParams.get("guestMobNo");
    
    this.reAllocate = this.navParams.get("reAllocate");
    if(this.reAllocate){
      this.currentGRO = this.navParams.get("currentGRO");
    }
    console.log(this.navParams.data)
    
  }

  reAllocateTo(groInfo){
    if(!this.reAllocate){
      this.allocateTo(groInfo);
      return
    }
    const confirm = this.alertCtrl.create({
      title: 'Alert',
      message: 'Are you sure want to Re-allocate to '+ groInfo.name +'?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            this.viewCtrl.dismiss();
          }
        },
        {
          text: 'Agree',
          handler: () => {
    
    var bookingDetailsPath = "/bookingDetails/"+this.service+"/"+this.bookingID+"/slotInfo/groInfo";
    var currentGROBookingPath = "/staffDetails/gro/"+this.currentGRO.uid+"/bookings/"+this.bookingID;
    var selectedGROBookingPath = "/staffDetails/gro/"+groInfo.uid+"/bookings/"+this.bookingID;
    var update = {};
    var epoch = moment.tz("Asia/Kolkata").valueOf();
    var bookingDetailsGROObj = {
      "assignDate": epoch,
      "email": groInfo.email,
      "mobileNo": groInfo.mobileNo,
      "name": groInfo.name,
      "uid": groInfo.uid
    }
    var groBookingObj = {
      "allocatedDetails":{"allocatedOn":epoch,"isRescheduled":true},
      "bookingID":this.bookingID,
      "mode":this.service,
      "status":"scheduled"
    }
    var prevGroPlayerID
    var reAllocatedGROPlayerID
    update[bookingDetailsPath] = bookingDetailsGROObj;
    update[currentGROBookingPath] = {};
    update[selectedGROBookingPath] = groBookingObj;
    console.log(update)
    this.afDB.object("/").update(update);
    var commsUrl = "https://us-central1-applied-craft-217711.cloudfunctions.net/hialms";
    // "admin":"mng.ghial@gmrgroup.in",
    // "manager":"amit.chandra@gmrgroup.in",
    var commsObj = {
      "mType":"reAllocateSlot",
      "admin":"mng.ghial@gmrgroup.in",
      "manager":"amit.chandra@gmrgroup.in",
      "previousGRO":this.currentGRO.name,
      "reAllocatedTo":groInfo.name,
      "bookingID":this.bookingID
    }
    console.log(commsObj)
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post(commsUrl, JSON.stringify(commsObj), { headers: headers })
      .pipe(map(res2 => res2.json()))
      .subscribe(data => {
        console.log(data);
      });
      this.viewCtrl.dismiss();

          }
        }
      ]
    });
    confirm.present();
  }

  allocateTo(groInfo){

    const confirm = this.alertCtrl.create({
      title: 'Alert?',
      message: 'Are you sure want to Allocate to '+groInfo.name+'?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            this.viewCtrl.dismiss();
          }
        },
        {
          text: 'Agree',
          handler: () => {
            var bookingDetailsPath = "/bookingDetails/"+this.service+"/"+this.bookingID+"/slotInfo/groInfo";
            var selectedGROBookingPath = "/staffDetails/gro/"+groInfo.uid+"/bookings/"+this.bookingID;
            var update = {};
            var epoch = moment.tz("Asia/Kolkata").valueOf();
            var bookingDetailsGROObj = {
              "assignDate": epoch,
              "email": groInfo.email,
              "mobileNo": groInfo.mobileNo,
              "name": groInfo.name,
              "uid": groInfo.uid
            }
            var groBookingObj = {
              "allocatedDetails":{"allocatedOn":epoch,"isRescheduled":false},
              "bookingID":this.bookingID,
              "mode":this.service,
              "status":"scheduled"
            }
            var prevGroPlayerID
            var reAllocatedGROPlayerID
            update[bookingDetailsPath] = bookingDetailsGROObj;
            update[selectedGROBookingPath] = groBookingObj;
            console.log(update)
            this.afDB.object("/").update(update);
            var commsUrl = "https://us-central1-applied-craft-217711.cloudfunctions.net/hialms";
            var commsObj = {
              "mType":"allocateSlot",
              "admin":"stalin@newgendigital.com",
              "manager":"senthil@newgendigital.com",
              "allocatedGRO":groInfo.name,
              "bookingID":this.bookingID,
              "slotTime":this.slotTime,
              "player_id":this.player_id,
              "guestMobNo":this.guestMobNo
            }
            console.log(commsObj)
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.http.post(commsUrl, JSON.stringify(commsObj), { headers: headers })
              .pipe(map(res2 => res2.json()))
              .subscribe(data => {
                console.log(data);
              });
              this.viewCtrl.dismiss();
          }
        }
      ]
    });
    confirm.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReallocateModalPage');
  }

}
