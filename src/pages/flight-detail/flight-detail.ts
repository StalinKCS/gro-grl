import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import * as moment from 'moment-timezone';
import { UtilsProvider } from '../../providers/utils/utils';

@Component({
  selector: 'page-flight-detail',
  templateUrl: 'flight-detail.html',
})
export class FlightDetailPage {
  searchResult: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public utils: UtilsProvider) {
    this.searchResult = this.navParams.get('result');
    console.log(this.searchResult);
    //  Optimization Reminder: Refactor appropriately
    let loader = this.utils.makeLoader('Please wait...');
    loader.present();
    if (this.searchResult["m"] === "arrival") {
      var arrStatusCodes = {"ARR":"ARRIVED", "ASK":"ASK", "CNL":"CANCELLED", "DEL":"DELAY", "DIV":"DIVERTED", "ETA":"EXPECTED", "FBA":"FIRST BAG", "LBA":"LAST BAG", "NWS":"NEXT INFO", "NOP":"NON OPERATION", "RER":"RE ROUTED", "":"EXPECTED"};
      this.searchResult["flStatus"] = arrStatusCodes[this.searchResult["status"]];
      var sch = this.searchResult["stoa"];
      //  20181121001000
      // 2018-11-21T00:10:00.000Z
      sch = sch.substring(0, 4) + "-" + sch.substring(4, 6) + "-" + sch.substring(6, 8) + "T" + sch.substring(8, 10) + ":" + sch.substring(10, 12) + ":" + sch.substring(12, 14) + ".000Z";
      this.searchResult["sch"] = moment.utc(sch).format("MMM DD YYYY [at] HH:mm A");
      if (this.searchResult["eta"] === "") {
        this.searchResult["eta"] = this.searchResult["sch"];
      } else {
        var eta = this.searchResult["eta"];
        eta = eta.substring(0, 4) + "-" + eta.substring(4, 6) + "-" + eta.substring(6, 8) + "T" + eta.substring(8, 10) + ":" + eta.substring(10, 12) + ":" + eta.substring(12, 14) + ".000Z";
        this.searchResult["eta"] = eta;
      }
    } else if (this.searchResult["m"] === "departure") {
      var depStatusCodes = {"AIB":"DEPARTED", "DIV":"DIVERTED", "NGT":"NEW GATE", "NWS":"NEXT INFO", "NOP":"NON OPERATION", "ETD":"ON TIME", "RER":"RE ROUTED", "RES":"RE SCHEDULED", "GTO":"GATE OPEN", "BRD":"BOARDING", "LCL":"FINAL CALL", "GCL":"GATE CLOSE", "DEL":"DELAY", "CNL":"CANCELLED", "HAJ":"HAJ TERMINA", "DEP":"DEPARTED", "":"ON TIME"}
      this.searchResult["flStatus"] = depStatusCodes[this.searchResult["status"]];
      var sch = this.searchResult["stod"];
      sch = sch.substring(0, 4) + "-" + sch.substring(4, 6) + "-" + sch.substring(6, 8) + "T" + sch.substring(8, 10) + ":" + sch.substring(10, 12) + ":" + sch.substring(12, 14) + ".000Z";
      this.searchResult["sch"] = moment.utc(sch).format("MMM DD YYYY [at] HH:mm A");
      if (this.searchResult["etd"] === "") {
        this.searchResult["eta"] = this.searchResult["sch"];
      } else {
        var eta = this.searchResult["etd"];
        eta = eta.substring(0, 4) + "-" + eta.substring(4, 6) + "-" + eta.substring(6, 8) + "T" + eta.substring(8, 10) + ":" + eta.substring(10, 12) + ":" + eta.substring(12, 14) + ".000Z";
        this.searchResult["eta"] = eta;
      }
    }
    loader.dismiss();
  }

  // ionViewDidLoad() {
  //   console.log(this.searchResult);

  // }

}
