import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SlotStatusPage } from '../slot-status/slot-status';

/**
 * Generated class for the GrlDashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-grl-dashboard',
  templateUrl: 'grl-dashboard.html',
})
export class GrlDashboardPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GrlDashboardPage');
  }
  gotonext(){
    this.navCtrl.push(SlotStatusPage);
  }

}
