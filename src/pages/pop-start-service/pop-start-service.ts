import { Component, Renderer } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, Events } from 'ionic-angular';
import { TrackmapPage } from '../trackmap/trackmap';


@Component({
  selector: 'page-pop-start-service',
  templateUrl: 'pop-start-service.html',
})
export class PopStartServicePage {
  serviceOTP: any;
  guestOTP: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public renderer: Renderer,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public events: Events
  ) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'modal-otp', true);
    this.serviceOTP = this.navParams.get("serviceOTP");
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PopStartServicePage');
  }
  closeModal() {
    this.viewCtrl.dismiss();
  }
  // nextpage(){
  //   this.navCtrl.push(TrackmapPage);
  // }

  verifyOTP() {
    console.log(this.guestOTP);
    console.log(this.serviceOTP);
    if (this.guestOTP) {
      if (this.guestOTP.toLowerCase() === this.serviceOTP.toLowerCase()) {
        this.events.publish("otpVerification", true);
        this.viewCtrl.dismiss();
        // let alert = this.alertCtrl.create({
        //   title: 'OTP VERIFIED SUCCESSFULLY',
        //   buttons: ['Dismiss']
        // });
        // alert.present();
      } else {
        this.events.publish("otpVerification", false);
        this.viewCtrl.dismiss();
        let alert = this.alertCtrl.create({
          title: 'ENTERED OTP IS WRONG',
          buttons: ['Dismiss']
        });
        alert.present();
      }
    }
    else {
      this.events.publish("otpVerification", false);
      this.viewCtrl.dismiss();
      let alert = this.alertCtrl.create({
        title: 'Please enter the valid service OTP',
        buttons: ['Dismiss']
      });
      alert.present();
    }
  }
}
