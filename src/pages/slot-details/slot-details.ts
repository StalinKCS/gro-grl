import { Component, Directive, Output, EventEmitter, Input, SimpleChange } from '@angular/core';
import { NavController, NavParams, AlertController, ModalController, Modal, ModalOptions } from 'ionic-angular';
import { SlotStatusPage } from '../slot-status/slot-status';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { ReallocateModalPage } from '../reallocate-modal/reallocate-modal';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { TrackmapPage } from '../trackmap/trackmap';

/**
 * Generated class for the SlotDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-slot-details',
  templateUrl: 'slot-details.html',
})
export class SlotDetailsPage {
  grlID: string;
  slotsList = [];
  slotsList_1 = [];
  groList = [];
  dash_overview = "gro";
  shownGroup = null;
  shownSlot = null;
  slots_overview = "scheduledSlots";

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private afDB: AngularFireDatabase,
    private afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController) {
    this.afAuth.authState.subscribe((auth) => {
      if (auth) {
        this.grlID = auth.uid;
        var bookingDetailsPath = "/bookingDetails";
        var groListPath = "/staffDetails/gro";
        this.afDB.list(groListPath).valueChanges().subscribe(res => {
          this.groList = res;
          //console.log(this.groList);
        })
        this.afDB.object(bookingDetailsPath).valueChanges().subscribe(res => {
          Object.keys(res).forEach(key => {

            if (key != "departurePrivilegePlan") {
              var mode = res[key];
              Object.keys(mode).forEach(key => {
                var status = mode[key]["status"];
                mode[key]["bookingID"] = key;
                this.slotsList.push(mode[key]);
                this.slotsList_1 = this.slotsList;
                if ((mode[key]["addOnDetails"]) && (mode[key]["addOnDetails"].length > 0)) {
                  mode[key]["addOnDetails"] = this.convertToLowerCase(mode[key]["addOnDetails"]);
                }
              })
            }
          })
        })
      }

    })
  }


  guestCount(a, c, i) {
    var totalGuest = 0;
    totalGuest = parseInt(a) + parseInt(c) + parseInt(i);
    return totalGuest;
  }


  convertToLowerCase(addOnArray) {
    var a;
    var newArray = []
    for (a of addOnArray) {
      newArray.push(a.toLowerCase());
    }
    return newArray;
  }

  allocateSlot(item) {
    var bookingID = item.bookingID;
    var service = item.service;
    var groDetails = []
    for (var i = 0; i < this.groList.length; i++) {
      if (this.groList[i]["isActivated"]) {
        groDetails.push(this.groList[i])
      }
    }
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: true,
    };

    const myModal: Modal = this.modalCtrl.create(
      ReallocateModalPage,
      {
        "groList": groDetails,
        "bookingID": bookingID,
        "service": service,
        "slotTime": item["slotInfo"].slotTimeLabel1 + " " + item["slotInfo"].slotTimeLabel2,
        "player_id": item["playerID"],
        "guestMobNo": item["receiptMobile"],
        "reAllocate": false
      },
      myModalOptions);

    myModal.present();

  }

  reAllocateSlot(item) {
    console.log(item);
    var bookingID = item.bookingID;
    var groInfo = item.slotInfo.groInfo;
    var service = item.service;
    var groID = groInfo.uid;
    var groDetails = [];
    for (var i = 0; i < this.groList.length; i++) {
      if ((this.groList[i]["uid"] != groID) && (this.groList[i]["isActivated"])) {
        groDetails.push(this.groList[i])
      }
    }
    const myModalOptions: ModalOptions = {
      enableBackdropDismiss: true
    };

    const myModal: Modal = this.modalCtrl.create(
      ReallocateModalPage,
      {
        "groList": groDetails,
        "bookingID": bookingID,
        "service": service,
        "currentGRO": groInfo,
        "reAllocate": true
      },
      myModalOptions);

    myModal.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DashGrlPage');
  }
  gotonext() {
    this.navCtrl.push(SlotStatusPage);
  }


  trackGuest(status, serviceList, bookingId, firstName, lastName, mobileNo) {
    this.navCtrl.push(TrackmapPage, { "bookingStatus": status, "serviceList": serviceList, "bookingID": bookingId, "guestName": firstName + " " + lastName, "guestPhone": mobileNo });
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };
  isGroupShown(group) {
    return this.shownGroup === group;
  };

  toggleSlot(group) {
    if (this.isSlotShown(group)) {
      this.shownSlot = null;
    } else {
      this.shownSlot = group;
    }
  };

  isSlotShown(group) {
    return this.shownSlot === group;
  };

  slotCount(groID, slotType) {
    var slotTypeId = slotType + groID;
    console.log(slotTypeId);
  }

  slotChk(groID, slotType) {
    console.log(groID, slotType)
  }

  getItems(ev: any) {
    var vm = this;
    // set val to the value of the searchbar
    const val = ev.target.value;
    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.slotsList = this.slotsList.filter((item) => {
        console.log(item);
        return (item.bookingID.toLowerCase().indexOf(val.toLowerCase()) > -1 || (item.guestInfo && item.guestInfo.guestInfo[0].firstName.toLowerCase().indexOf(val.toLowerCase())) > -1);
      })
    }
    else {
      this.slotsList = [];
      this.slotsList = this.slotsList_1;
    }
  }

}
