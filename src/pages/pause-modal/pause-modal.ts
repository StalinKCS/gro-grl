import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController  } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as moment from 'moment-timezone';
import { Http, RequestOptions, Request, RequestMethod, Headers } from '@angular/http';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';
import { map } from 'rxjs/operators';

/**
 * Generated class for the PauseModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-pause-modal',
  templateUrl: 'pause-modal.html',
})
export class PauseModalPage {
  bookingID:any;
  groName:any;
  groID:any;
  mode:any;
  otherReason:any;
  shownGroup = null;
  msg:any;
  adminEmail = "mng.ghial@gmrgroup.in";
  hours:any;
  reason:any;
  commsUrl = "https://us-central1-applied-craft-217711.cloudfunctions.net/hialms";

  constructor(
                public navCtrl: NavController, 
                public alertCtrl: AlertController,
                public navParams: NavParams,
                private afDB: AngularFireDatabase, 
                public viewCtrl: ViewController,
                public http: Http
              ) {
    this.bookingID = this.navParams.get("bookingID");
    this.groName = this.navParams.get("groName");
    this.groID = this.navParams.get("groID");
    this.mode = this.navParams.get("mode");
    this.hours = "1";
  }

  informGRL(){
    const confirm = this.alertCtrl.create({
      title: 'Alert?',
      message: 'Are you sure want to Pause Service?',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Agree',
          handler: () => {
            var commsObj = {"mType":"pauseService","bookingID":this.bookingID,"groName":this.groName,"noOfHours":this.hours};
            var grlDetailsPath = "/staffDetails/grl/";
            var bookingDetailsPath = "/bookingDetails/"+this.mode+"/"+this.bookingID;
            var payLoad = {
              "pauseInfo" : {
                "hours": this.hours,
                "remarks":this.reason?this.reason:'',
                "createdAt": new Date().getTime(),
                "paused":true
              }
            }
            //console.log(payLoad);
            this.afDB.object(bookingDetailsPath).update(payLoad);
            // // Comment out the below block from text "start" to "end" before sending to client
            // Start
            this.afDB.object(grlDetailsPath).valueChanges().subscribe(res => {
              var omitList = ["gmrprimegrl@hial.com","raj@gmail.com","grl@hial.in","logesh@gmrprime.in"];
              var emailList = [this.adminEmail];
              var mobileList = [];
              Object.keys(res).forEach(key => {
                var email = res[key]["email"];
                var mobile = res[key]["mobileNo"];
                if((res[key]["isActivated"]) && (omitList.indexOf(email) != -1)){
                  emailList.push(email);
                  mobileList.push(mobile);
                }
              })
              commsObj["emaiList"] = emailList;
              commsObj["mobileList"] = mobileList;
              var headers = new Headers();
              headers.append('Content-Type', 'application/json');
              this.http.post(this.commsUrl, JSON.stringify(commsObj), { headers: headers })
                .pipe(map(res2 => res2.json()))
                .subscribe(data => {
                  console.log(data);
                });
            })
            // End
        
            // Comment code from text "start" to "end" before taking a build and pushing the apk to cloud storage
            // Start
        
            // commsObj["emailList"] = ["stalin@newgendigital.com","stalin@epagemaker.com"];
            // commsObj["mobileList"] = ["8939484295"];
            // console.log(commsObj)
            // var headers = new Headers();
            // headers.append('Content-Type', 'application/json');
            // this.http.post(this.commsUrl, JSON.stringify(commsObj), { headers: headers })
            //   .pipe(map(res2 => res2.json()))
            //   .subscribe(data => {
            //     console.log(data);
            //   });
            // End
            this.viewCtrl.dismiss();
          }
        }
      ]
    });
    confirm.present();



  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  };

  isGroupShown(group) {
    return this.shownGroup === group;
  };

  ionViewDidLoad() {
    console.log('ionViewDidLoad PauseModalPage');
  }

}
