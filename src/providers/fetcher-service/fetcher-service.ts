import { Injectable } from '@angular/core';
import { Http, Headers,RequestOptions } from '@angular/http';
import { environment } from '../../environments/environment';
// import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
  Generated class for the FetcherServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FetcherServiceProvider {
  data: any;
  private baseUrl: string = environment.firebase.baseUrl;
  constructor(public http: Http) {
    console.log('Hello FetcherServiceProvider Provider');
  }
  maxDate(body) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new Promise(resolve => {
      let url = this.baseUrl + 'maxDate';
      // console.log(url);
      this.http.post(url, body, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          // console.log(JSON.parse(JSON.stringify(data || null )));
          resolve(data);
        });
    });
  }
  getFlightStatus(serviceType, travelMode, flightNum, cityName) {
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', "X-Requested-With, Content-Type, Origin, Authorization, Accept, Client-Security-Token, Accept-Encoding");
    let options = new RequestOptions({ headers: headers });
    return new Promise(resolve => {
      let url = "http://www.hyderabad.aero/gmrprimeapi/liveflightinfo.aspx?FltType=" + serviceType + "&FltWay=" + travelMode + "&FltNum=" + flightNum + "&FltFrom=" + cityName;
      this.http.get(url,options)
        .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        });
    }).then(function (resp) {
      return resp;
    },
      function (error) {
        return error;
      });
  }
  flStatus(body) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new Promise(resolve => {
      let url = this.baseUrl + 'flStatus';
      this.http.post(url, body, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          //console.log(JSON.parse(JSON.stringify(data || null )));
          resolve(data);
        });
    });
  }

  sendOtp(body) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new Promise(resolve => {
      let url = this.baseUrl + 'hialms';
      this.http.post(url, body, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          //console.log(JSON.parse(JSON.stringify(data || null )));
          resolve(data);
        });
    });
  }

  verifyOtp(body) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new Promise(resolve => {
      let url = this.baseUrl + 'hialms';
      this.http.post(url, body, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          //console.log(JSON.parse(JSON.stringify(data || null )));
          resolve(data);
        });
    });
  }

  pushNotification(body) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return new Promise(resolve => {
      let url = 'https://onesignal.com/api/v1/notifications';
      this.http.post(url, body, { headers: headers })
        .map(res => res.json())
        .subscribe(data => {
          //console.log(JSON.parse(JSON.stringify(data || null )));
          resolve(data);
        });
    });
  }

}
