
import { Injectable } from '@angular/core';
import { AlertController,LoadingController} from 'ionic-angular';
import { Toast } from '@ionic-native/toast';



/*
  Generated class for the UtilsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UtilsProvider {
  public statusBarShow;

  constructor( public alertCtrl: AlertController, public toast : Toast,public loader: LoadingController ) {
    console.log('Hello UtilsProvider Provider');
    
  }
  makeAlert(title, msg) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    return alert;
  }

  makeToast(msg){
    this.toast.show(msg, '3000', 'bottom').subscribe(toast => {});
  }
  isCordovaAvailable()
  {
    if (!(<any>window).cordova) {
      console.log('This is a native feature. Please use a device');
      return false;
    }
    return true;
  }

  makeLoader(msg){
    let loader =  this.loader.create({
      content: msg
    });
    return loader;

  }
 

}
