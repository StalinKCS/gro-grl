
// import { MgLoginPage } from './../pages/mg-login/mg-login';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController, AlertController, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { HomePage } from '../pages/home/home';
import { MgLoginPage } from '../pages/mg-login/mg-login';
import { DashboardGroPage } from '../pages/dashboard-gro/dashboard-gro';
import { DashGroPage } from '../pages/dash-gro/dash-gro';
import { DashGrlPage } from '../pages/dash-grl/dash-grl';
// import { RatingServicePage } from '../pages/rating-service/rating-service';
import { ReportsPage } from '../pages/reports/reports';
// import { PopStartServicePage } from '../pages/pop-start-service/pop-start-service';
// import { PopMeetGreetPage } from '../pages/pop-meet-greet/pop-meet-greet';
// import { TrackmapPage } from '../pages/trackmap/trackmap';
import { FlightstatusPage } from './../pages/flightstatus/flightstatus';
import { AngularFireAuth } from 'angularfire2/auth';
import { TrackmapPage } from '../pages/trackmap/trackmap';
import { TrackGuestPage } from '../pages/track-guest/track-guest';
// import { GrlDashboardPage } from '../pages/grl-dashboard/grl-dashboard';
import { SlotStatusPage } from '../pages/slot-status/slot-status';
import { ExecutiveDetailsPage } from '../pages/executive-details/executive-details';
import { SlotDetailsPage } from '../pages/slot-details/slot-details';
import { FeedbackPage } from '../pages/feedback/feedback';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { ChangePasswordPage } from '../pages/change-password/change-password';
import { PlacardPage } from '../pages/placard/placard';
import { UtilsProvider } from '../providers/utils/utils';
import { environment } from '../environments/environment';
import { RatingServicePage } from '../pages/rating-service/rating-service';

import { AndroidPermissions } from '@ionic-native/android-permissions';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any;
  menuitem: any;
  public userObj: any;
  name: any;
  email: any;
  role: any;
  constructor(public utils: UtilsProvider,
    private afDB: AngularFireDatabase,
    public afAuth: AngularFireAuth,
    public alertCtrl: AlertController,
    public app: App,
    public platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public menu: MenuController,
    public androidPermissions: AndroidPermissions) {
    let loader = this.utils.makeLoader('Please wait');
    loader.present();
    this.afAuth.authState.subscribe((loggedInUser) => {
      var versionPath = "/appsVersion/grlgroApp";
      this.afDB.object(versionPath).valueChanges().subscribe(res => {
        if (res["currentVersion"] != environment.firebase.version && this.utils.isCordovaAvailable()) {
          loader.dismiss();
          let alert = this.alertCtrl.create({
            title: 'New version available',
            enableBackdropDismiss: false,
            message: 'Please,update app to new version',
            buttons: [
              {
                text: 'UPDATE',
                handler: () => {
                  platform.exitApp(); // Close this application
                  window.open('https://play.google.com/apps/testing/io.GMR.PRIME', '_system');
                }
              }
            ]
          });
          alert.present();
        }
        else {
          if (loggedInUser == null) {
            loader.dismiss();
            this.rootPage = MgLoginPage;
          } else {
            console.log(loggedInUser.uid)
            var roleInfoPath = 'staffDetails/roleInfo/staffs/' + loggedInUser.uid;
            this.afDB.object(roleInfoPath).valueChanges().subscribe(res => {
              var staffDetailPath = 'staffDetails/' + res + "/" + loggedInUser.uid;
              this.afDB.object(staffDetailPath).valueChanges().subscribe(res => {
                this.name = res?res["name"]:"";
                loader.dismiss();
                if ((res) && (res["isActivated"])) {
                  console.log(res["role"])
                  if (res["role"] == 'gro') {
                    this.menuitem = [
                      // { title: 'Home', component: DashboardGroPage, icon: "assets/imgs/blue-icon-dashboard.png" },
                      { title: 'Dashboard', component: DashGroPage, icon: "assets/imgs/blue-icon-dashboard.png" },
                      // { title: 'Reports', component: ReportsPage, icon: "assets/imgs/blue-icon-info.png" },
                      { title: 'Flight Status', component: FlightstatusPage, icon: "assets/imgs/blue-icon-flight.png" }
                    ];
                    if (localStorage.getItem("otp")) {
                      this.rootPage = DashGroPage;
                    }
                    else {
                      this.rootPage = MgLoginPage;
                    }
                  }
                  else if (res["role"] == 'grl') {
                    this.menuitem = [
                      // { title: 'Home', component: GrlDashboardPage, icon: "assets/imgs/blue-icon-dashboard.png" },
                      // { title: 'Dashboard', component: DashGrlPage, icon: "assets/imgs/blue-icon-dashboard.png" },
                      { title: 'GRO Details', component: ExecutiveDetailsPage, icon: "assets/imgs/blue-icon-info.png" },
                      { title: 'Slot Details', component: SlotDetailsPage, icon: "assets/imgs/blue-icon-info.png" },
                      { title: 'Flight Status', component: FlightstatusPage, icon: "assets/imgs/blue-icon-flight.png" }
                    ];
                    if (localStorage.getItem("otp")) {
                      //this.rootPage = DashGrlPage;
                      this.rootPage = ExecutiveDetailsPage;
                    }
                    else {
                      this.rootPage = MgLoginPage;
                    }

                  }
                }
                else {
                  this.rootPage = MgLoginPage;
                }
              })
            })

          }
        }
      })
    });


    // if (!this.platform.is('tablet')) {
    //   loader.dismiss();
    //   let alert = this.alertCtrl.create({
    //     title: 'Not Allowed',
    //     enableBackdropDismiss: false,
    //     message: 'Please use this App from your tablet',
    //     // buttons: ['Dismiss']
    //   });
    //   alert.present();
    // } else {
    //   this.afAuth.authState.subscribe((loggedInUser) => {
    //     var versionPath = "/appsVersion/grlgroApp";
    //     this.afDB.object(versionPath).valueChanges().subscribe(res => {
    //       if (res["currentVersion"] != environment.firebase.version && this.utils.isCordovaAvailable()) {
    //         loader.dismiss();
    //         let alert = this.alertCtrl.create({
    //           title: 'New version available',
    //           enableBackdropDismiss: false,
    //           message: 'Please,update app to new version',
    //           buttons: [
    //             {
    //               text: 'UPDATE',
    //               handler: () => {
    //                 platform.exitApp(); // Close this application
    //                 window.open('https://play.google.com/apps/testing/io.GMR.PRIME', '_system');
    //               }
    //             }
    //           ]
    //         });
    //         alert.present();
    //       }
    //       else {
    //         if (loggedInUser == null) {
    //           loader.dismiss();
    //           this.rootPage = MgLoginPage;
    //         } else {
    //           console.log(loggedInUser.uid)
    //           var roleInfoPath = 'staffDetails/roleInfo/staffs/' + loggedInUser.uid;
    //           this.afDB.object(roleInfoPath).valueChanges().subscribe(res => {
    //             var staffDetailPath = 'staffDetails/' + res + "/" + loggedInUser.uid;
    //             this.afDB.object(staffDetailPath).valueChanges().subscribe(res => {
    //               loader.dismiss();
    //               if ((res) && (res["isActivated"])) {
    //                 console.log(res["role"])
    //                 if (res["role"] == 'gro') {
    //                   this.menuitem = [
    //                     // { title: 'Home', component: DashboardGroPage, icon: "assets/imgs/blue-icon-dashboard.png" },
    //                     { title: 'Dashboard', component: DashGroPage, icon: "assets/imgs/blue-icon-dashboard.png" },
    //                     // { title: 'Reports', component: ReportsPage, icon: "assets/imgs/blue-icon-info.png" },
    //                     { title: 'Flight Status', component: FlightstatusPage, icon: "assets/imgs/blue-icon-flight.png" }
    //                   ];
    //                   this.rootPage = DashGroPage;
    //                 }
    //                 else if (res["role"] == 'grl') {
    //                   this.menuitem = [
    //                     // { title: 'Home', component: GrlDashboardPage, icon: "assets/imgs/blue-icon-dashboard.png" },
    //                     { title: 'Dashboard', component: DashGrlPage, icon: "assets/imgs/blue-icon-dashboard.png" },
    //                     // { title: 'GRO Details', component: ExecutiveDetailsPage, icon: "assets/imgs/blue-icon-info.png" },
    //                     { title: 'Flight Status', component: FlightstatusPage, icon: "assets/imgs/blue-icon-flight.png" }
    //                   ];
    //                   this.rootPage = DashGrlPage;
    //                 }
    //               }
    //               else {
    //                 this.rootPage = MgLoginPage;
    //               }
    //             })
    //           })

    //         }
    //       }
    //     })
    //   });
    // }


    platform.ready().then(() => {
      setTimeout(() => {
      androidPermissions.checkPermission(androidPermissions.PERMISSION.CALL_PHONE).then((result) => {
        console.log('Has permission?PERMISSION.CALL_PHONE', result.hasPermission);
        if (!result.hasPermission) {
          androidPermissions.requestPermission(androidPermissions.PERMISSION.CALL_PHONE)
        }
      });
      androidPermissions.checkPermission(androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION).then((result) => {
        console.log('Has permission?ACCESS_COARSE_LOCATION', result.hasPermission);
        if (!result.hasPermission) {
          androidPermissions.requestPermission(androidPermissions.PERMISSION.ACCESS_COARSE_LOCATION)
        }
      });

      androidPermissions.checkPermission(androidPermissions.PERMISSION.ACCESS_FINE_LOCATION).then((result) => {
        console.log('Has permission?ACCESS_FINE_LOCATION', result.hasPermission);
        if (!result.hasPermission) {
          androidPermissions.requestPermission(androidPermissions.PERMISSION.ACCESS_FINE_LOCATION)
        }
      });

      androidPermissions.checkPermission(androidPermissions.PERMISSION.ACCESS_LOCATION_EXTRA_COMMANDS).then((result) => {
        console.log('Has permission?ACCESS_LOCATION_EXTRA_COMMANDS', result.hasPermission);
        if (!result.hasPermission) {
          androidPermissions.requestPermission(androidPermissions.PERMISSION.ACCESS_LOCATION_EXTRA_COMMANDS)
        }
      });
    }, 2000);
      platform.registerBackButtonAction(() => {
        let nav = app.getActiveNavs()[0];
        // let activeView = nav.getActive();
        if (nav.canGoBack()) { //Can we go back?
          nav.pop();
        } else {
          const alert = this.alertCtrl.create({
            title: 'App termination',
            message: 'Do you want to close the app?',
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Application exit prevented!');
              }
            }, {
              text: 'Close App',
              handler: () => {
                this.platform.exitApp(); // Close this application
              }
            }]
          });
          alert.present();
        }
      });
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      this.menu.swipeEnable(false);
      splashScreen.hide();
    });
  }

  ionViewDidLoad() {

  }

  openPage(component, index) {
    this.nav.setRoot(component);
  }
  logOut() {
    var vm = this;
    localStorage.clear();
    vm.afAuth.auth.signOut()
      .then((res) =>
        vm.nav.setRoot(MgLoginPage));
  }
}

