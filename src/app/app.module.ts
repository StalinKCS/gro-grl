import { ForgotPwdPage } from './../pages/forgot-pwd/forgot-pwd';
import { ChangePasswordPage } from './../pages/change-password/change-password';
import { Network } from '@ionic-native/network';
import { FetcherServiceProvider } from './../providers/fetcher-service/fetcher-service';
import { FlightDetailPage } from './../pages/flight-detail/flight-detail';
import { FlightstatusPage } from './../pages/flightstatus/flightstatus';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Ionic2RatingModule} from 'ionic2-rating';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MgLoginPage } from '../pages/mg-login/mg-login';
import { DashboardGroPage } from '../pages/dashboard-gro/dashboard-gro';
import { GrlDashboardPage } from '../pages/grl-dashboard/grl-dashboard';
import { DashGroPage } from '../pages/dash-gro/dash-gro';
import { DashGrlPage } from '../pages/dash-grl/dash-grl';
import { RatingServicePage } from '../pages/rating-service/rating-service';
import { ReportsPage } from '../pages/reports/reports';
import { PrimePage } from '../pages/prime/prime';
import { PopStartServicePage } from '../pages/pop-start-service/pop-start-service';
import { PopMeetGreetPage } from '../pages/pop-meet-greet/pop-meet-greet';
import { TrackmapPage } from '../pages/trackmap/trackmap';
import { TrackGuestPage } from '../pages/track-guest/track-guest';
import { GoogleMaps } from '@ionic-native/google-maps';
import { UtilsProvider } from '../providers/utils/utils';
import { NgxSpinnerModule} from 'ngx-spinner';
import { MyngxSpinnerComponent } from '../components/myngx-spinner/myngx-spinner';
import { Toast } from '@ionic-native/toast';
import { HttpModule } from '@angular/http';
import { AngularFireModule } from 'angularfire2';
import { AngularFireAuthModule, AngularFireAuth } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { AngularFireDatabase } from 'angularfire2/database';

import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
// import { GrlDashboardPage } from '../pages/grl-dashboard/grl-dashboard';
import { SlotStatusPage } from '../pages/slot-status/slot-status';
import { ExecutiveDetailsPage } from '../pages/executive-details/executive-details';
import { SlotDetailsPage } from '../pages/slot-details/slot-details';
import { FeedbackPage } from '../pages/feedback/feedback';
import { PlacardPage } from '../pages/placard/placard';
import { OtpPage } from '../pages/otp/otp';
import { ReallocateModalPage } from '../pages/reallocate-modal/reallocate-modal';
import { PauseModalPage } from '../pages/pause-modal/pause-modal';
import { AndroidPermissions } from '@ionic-native/android-permissions';
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MgLoginPage,
    DashGroPage,
    DashGrlPage,
    PrimePage,
    DashboardGroPage,
    RatingServicePage,
    ReallocateModalPage,
    PauseModalPage,
    ReportsPage,
    PopStartServicePage,
    PopMeetGreetPage,
    TrackmapPage,
    TrackGuestPage,
    FlightstatusPage,
    FlightDetailPage,
    MyngxSpinnerComponent,
    ChangePasswordPage,
    ForgotPwdPage,
    GrlDashboardPage,
    SlotStatusPage,
    ExecutiveDetailsPage,
    SlotDetailsPage,
    FeedbackPage,
    PlacardPage,
    OtpPage
  ],
  imports: [
    BrowserModule,
    Ionic2RatingModule,
    NgxSpinnerModule,
    HttpModule,
    // IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MgLoginPage,
    DashGroPage,
    DashGrlPage,
    PrimePage,
    DashboardGroPage,
    RatingServicePage,
    ReallocateModalPage,
    PauseModalPage,
    ReportsPage,
    PopStartServicePage,
    PopMeetGreetPage,
    TrackmapPage,
    TrackGuestPage,
    FlightstatusPage,
    FlightDetailPage,
    ChangePasswordPage,
    ForgotPwdPage,
    GrlDashboardPage,
    SlotStatusPage,
    ExecutiveDetailsPage,
    SlotDetailsPage,
    FeedbackPage,
    PlacardPage,
    OtpPage
  ],
  providers: [
    StatusBar,
    SplashScreen,FetcherServiceProvider,UtilsProvider,Toast,Network,AngularFireAuth,AngularFireDatabase,
    Geolocation,
    Diagnostic,
    // FabContainer,
    GoogleMaps,
    LocationAccuracy,
    BackgroundGeolocation,
    AndroidPermissions,
    // CallNumberOriginal,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
